module Structs

    struct ParamSimul
        audioRate::Union{Int32,Float32};
        sampleRate::Union{Int32,Float32};
        nbChannels::Union{Int32,Float32};
        channelBW::Union{Int32,Float32};
        blackRate::Union{Int32,Float32};
        totalDuration::Union{Int32,Float32};
        oneTone::Union{Int32,Float32};
        modeTx::String;
        modeFH::String;        
        ARCHI::String;
    end
    export ParamSimul

    struct SynthSignals
        duration::Float64;
        blackRate::Float64;
        freqTones::Array{Float64};
        samplingRate::Float64;
        N:: Int;
        λ::Int;
        burstDuration::Float64;
        nbHops::Int;
        nbSymb::Int;
        sequence::Array{Int};
    end
    export SynthSignals


    struct PerfSave
        audioRate::Union{Int32,Float32};
        sampleRate::Union{Int32,Float32};
        nbChannels::Union{Int32,Float32};
        channelBW::Union{Int32,Float32};
        blackRate::Union{Int32,Float32};
        λRange::Union{Int32,Array{Int32},Array{Float32}};
        totalDuration::Union{Int32,Float32};
        pointPerBurst::Int32;
        modeTx::String;
        modeFH::String;
        hRange::Array{Union{Int32,Float32}};
        snrRange::Array{Union{Int32,Float32}};
        cfoRange::Array{Union{Int32,Float32}};
        delayRange::Array{Union{Int32,Float32}};
        KRange::Array{Union{Int32,Float32}};
        NRange::Array{Union{Int32,Float32}};
        sleepOffRange::Array{Union{Int32,Float32}};
        sleepOnRange::Array{Union{Int32,Float32}};
        algoRange::Array{Union{Int32,Float32}};
        nbIt::Int32;
        redData::Array{Union{Int32,Float32}};
        hatRedData::Array{Union{Int32,Float32}};
        CER::Array{Union{Int32,Float32}};
        SBOS::Array{Union{Int32,Float32}};
        ODG::Array{Union{Int,Float32}};
        BandwidthRef::Array{Union{Int,Float32}};
        BandwidthTest::Array{Union{Int,Float32}};
        totalNMR::Array{Union{Int,Float32}};
        WinModDiff1::Array{Union{Int,Float32}};
        ADB::Array{Union{Int,Float32}};
        EHS::Array{Union{Int,Float32}};
        AvgModDiff1::Array{Union{Int,Float32}};
        AvgModDiff2::Array{Union{Int,Float32}};
        RmsNoiseLoud::Array{Union{Int,Float32}};
        MFPD::Array{Union{Int,Float32}};
        RelDistFrames::Array{Union{Int,Float32}};
        vectFH::Array{Int32};
        vectEstim::Array{Float32};
    end
    export PerfSave


end
