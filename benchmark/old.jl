 

function plotPerfODG(perf1)

	# --- Figure
	@pgf plt = Axis({
							height      = "3in",
							width       = "4in",
							grid, 
							yminorgrids,
							xminorgrids,
							xlabel      = "SNR [dB]",
							ylabel      = "ODG",
							#ylabel      = "NMR",
							#ylabel      = "ADB",
							#ylabel      = "EHS",
							#ylabel      = "Visqol",							
							#title       = "Channel error rate, GFSK, 3 tone (h 0.01), 250 ps async",
							#title       = "Channel error rate, no modulation, in sync",
							#title       = "Objective Difference Grade  \$ \\in \$ [-4 0], GFSK, 3 tone N 40, \$ \\lambda \$ = K = 20, in sync",
							#title       = "Noise-to-mask ratio \$ \\in \$ [-24.045116 16.212030], GFSK, 3 tone N 40, \$ \\lambda \$ = K = 20, in sync",
							title       = "Visqol, GFSK, 3 tone N 80, \$ \\lambda \$ = K = 20, in sync",
							#title       = "Average block distortion \$ \\in \$ [-0.206623 2.886017], GFSK, 3 tone N 40, \$ \\lambda \$ = K = 100, in sync",
							#title       = "Harmonic structure of the error \$ \\in \$ [0.074318 13.933351], GFSK, 3 tone N 40, \$ \\lambda \$ = K = 100, in sync",							
							#ymin		= 1e-5,	
							xmin 		= -35,
							xmax 		= 0,
							legend_style= "{at={(1.1,-0.1)},anchor=south west,legend cell align=left,align=left,draw=white!15!black}"
							},	);

	# Output1[delayRange,burstRange,hRange,snrRange,cfoRange,λRange,windowRange,overRange,sleepRange,algoRange,:]
	sync = 1
	bur = 1

	temp1 = mean((perf1.Output2), dims=11)
	temp2 = mean((perf1.Output2), dims=11)
	linestyle = ["{thick},{loosely dotted}" "{thick},{densely dotted}" "{solid}" "{dashed}" "{thick},{solid}"]

	algo = ["DFT N" "FFT 1024" "PPN N" "Wavelet N"]
	for ii = 1:1:length(perf1.algoRange)
		
		leg = "$(algo[ii])";	
		col	  = getColorLatex(ii);
		mar	  = getMarkerLatex(1);
		push!(plt,@pgf PGFPlotsX.Plot({color=col,mark="no mark", style="{ultra thick},{solid}"},Table([ [-100.0] , [1e-5] ])));
		push!(plt,@pgf LegendEntry(leg));
	end
	
	for ii = 1:1:length(perf1.hRange)-1
		leg = "h $(perf1.hRange[ii]) ";	
		col	  = getColorLatex(6);
		mar	  = getMarkerLatex(1);
		push!(plt,@pgf PGFPlotsX.Plot({color=col,mark="no mark", style=linestyle[ii]},Table([ [-100.0] , [1e-5] ])));
		push!(plt,@pgf LegendEntry(leg));
	end



	#temp1 .= max.(temp1, 9e-6)
	#temp2 .= max.(temp2, 9e-6)
	#test = @pgf PlotlyBase.GenericTrace{Dict{Symbol,Any}}[]

	for ii = 1:1:length(perf1.hRange)-1
		temp4 = Array{eltype(temp1)}(undef, length(perf1.snrRange))
		#print("$(perf1.snrRange[ii]) dB : $(mean(skipmissing(temp2[1,1,1,ii,:,:,:,:,:,:])))\n")

		for ik = 1:1:length(perf1.snrRange)
			temp4[ik]= temp1[sync,bur,ii,ik,1,1,1,1,1,1];
		end	
		col	  = getColorLatex(1);
		mar	  = getMarkerLatex(ii);
		#leg = "\$ \\lambda \$ $(Int(perf1.burstRange[ii])) DFT N (K 32)"; 
		#leg = "DFT N (h $(perf1.hRange[ii]))";
		push!(plt, @pgf PGFPlotsX.Plot({color=col,mark="no mark", style=linestyle[ii]},Table([perf1.snrRange,temp4])));
		#push!(plt,@pgf LegendEntry(leg));
	end

	for ii = 1:1:length(perf1.hRange)-1
		temp4 = Array{eltype(temp1)}(undef, length(perf1.snrRange))
		#print("$(perf1.snrRange[ii]) dB : $(mean(skipmissing(temp2[1,1,1,ii,:,:,:,:,:,:])))\n")

		for ik = 1:1:length(perf1.snrRange)
			temp4[ik]= temp1[sync,bur,ii,ik,1,1,1,1,1,2];
		end	
		col	  = getColorLatex(2);
		mar	  = getMarkerLatex(ii);
		#leg = "FFT 1024 (h $(perf1.hRange[ii]))";
		push!(plt, @pgf PGFPlotsX.Plot({color=col,mark="no mark", style=linestyle[ii]},Table([perf1.snrRange,temp4])));
		#push!(plt,@pgf LegendEntry(leg));		
	end

	for ii = 1:1:length(perf1.hRange)-1
		temp4 = Array{eltype(temp1)}(undef, length(perf1.snrRange))
		#print("$(perf1.snrRange[ii]) dB : $(mean(skipmissing(temp2[1,1,1,ii,:,:,:,:,:,:])))\n")

		for ik = 1:1:length(perf1.snrRange)
			temp4[ik]= temp1[sync,bur,ii,ik,1,1,1,1,1,3]; #temp1[sync,ii,1,ik,1,1,1,1,1,3];
		end	
		col	  = getColorLatex(3);
		mar	  = getMarkerLatex(ii);
		#leg = "\$ \\lambda \$ $(Int(perf1.burstRange[ii])) PPN N (K 32)";
		#leg = "PPN N (h $(perf1.hRange[ii]))";
		push!(plt, @pgf PGFPlotsX.Plot({color=col,mark="no mark", style=linestyle[ii]},Table([perf1.snrRange,temp4])));
		#push!(plt,@pgf LegendEntry(leg));
	end

	for ii = 1:1:length(perf1.hRange)-1
		temp4 = Array{eltype(temp1)}(undef, length(perf1.snrRange))
		#print("$(perf1.snrRange[ii]) dB : $(mean(skipmissing(temp2[1,1,1,ii,:,:,:,:,:,:])))\n")

		for ik = 1:1:length(perf1.snrRange)
			temp4[ik]= temp1[sync,bur,ii,ik,1,1,1,1,1,4];
		end	
		col	  = getColorLatex(4);
		mar	  = getMarkerLatex(ii);
		#leg = "\$ \\lambda \$ $(Int(perf1.burstRange[ii])) Wavelet N (K 32)";
		#leg = "Wavelet N (h $(perf1.hRange[ii]))";
		push!(plt, @pgf PGFPlotsX.Plot({color=col,mark="no mark", style=linestyle[ii]},Table([perf1.snrRange,temp4])));
		#push!(plt,@pgf LegendEntry(leg));
	end	


	display(plt);
	pgfsave("plots.pdf", plt)	
end


function plotPerfPPN2(perf1)
    @pgf plt = SemiLogYAxis({
        height      = "3in",
        width       = "4in",
        grid, 
        yminorgrids,
        xminorgrids,
        xlabel      = "SNR [dB]",
        ylabel      = "Channel Error Rate",
        #title       = "Channel error rate, GFSK, 3 tone (h 0.01), 250 ps async",
        title       = "Channel error rate, PPN chanalyzer detector", 
        #title       = "Channel error rate, GFSK, 3 tone (h 0.01), in sync",
        ymin		= 1e-5,	
        xmin 		= -35,
        legend_style= "{at={(1.1,0)},anchor=south west,legend cell align=left,align=left,draw=white!15!black}"
        },	);
    # Output1[delayRange,burstRange,hRange,snrRange,cfoRange,λRange,windowRange,overRange,sleepRange,algoRange,:]
    
    
    temp1 = mean((perf1.Output1), dims=11)
    temp2 = mean((perf1.Output2), dims=11)
    
    #temp1 .= max.(temp1, 9e-6)
    #temp2 .= max.(temp2, 9e-6)
    linestyle = ["{ultra thick},{dashed}" "{ultra thick},{dotted}" "{solid}" "{solid}" "{thick},{solid}"]
    
    windoww = ["cosine" "kaiser" "gauss 0.1" "gauss 0.33" "gaussian 0.5"]
    mode = ["Raw" "Low pass" "FFT"]
    
    
    for ii = 1:1:length(perf1.burstRange)
        leg = "\$ \\lambda \$ $(Int(perf1.burstRange[ii]))";	
        col	  = getColorLatex(ii);
        mar	  = getMarkerLatex(1);
        push!(plt,@pgf PGFPlotsX.Plot({color=col,mark="no mark", style="{ultra thick},{solid}"},Table([ [-100.0] , [1e-5] ])));
        push!(plt,@pgf LegendEntry(leg));
    end
    
    for ii = 1:1:length(perf1.λRange)
        leg = "K $(perf1.λRange[ii]) ";	
        col	  = getColorLatex(6);
        mar	  = getMarkerLatex(1);
        push!(plt,@pgf PGFPlotsX.Plot({color=col,mark="no mark", style=linestyle[ii]},Table([ [-100.0] , [1e-5] ])));
        push!(plt,@pgf LegendEntry(leg));
    end
    
    
    
    #=
    for ij = [1 2 5]
        for ii = [3]
            leg = "$(windoww[ij])";	
            col	  = getColorLatex(6);
            mar	  = getMarkerLatex(ii);
            push!(plt,@pgf PGFPlotsX.Plot({color=col,mark="no mark", style=linestyle[ij]},Table([ [-100.0] , [1e-5] ])));
            push!(plt,@pgf LegendEntry(leg));
        end
    end
    for ii = 1:1:length(perf1.overRange)
        leg = " $(mode[ii])";	
        col	  = getColorLatex(1);
        mar	  = getMarkerLatex(ii);
        push!(plt,@pgf PGFPlotsX.Plot({color=col,mark=mar, style="{ultra thick},{only marks}"},Table([ [-100.0] , [1e-5] ])));
        push!(plt,@pgf LegendEntry(leg));
    end
    
    =#
    
    for ib = 1:1:length(perf1.λRange)	
        for ij = [1]#1:1:length(perf1.overRange)
            for ii = 1:1:length(perf1.burstRange)#
                temp4 = Array{eltype(temp1)}(undef, length(perf1.snrRange))
                #print("$(perf1.snrRange[ii]) dB : $(mean(skipmissing(temp2[1,1,1,ii,:,:,:,:,:,:])))\n")
    
                for ik = 1:1:length(perf1.snrRange)
                    temp4[ik]= temp1[1,ii,1,ik,1,ib,1,ij,1,1]; #temp1[sync,ii,1,ik,1,1,1,1,1,3];
                    temp4[ik] = max(temp4[ik], 9e-6)
                end	
    
                col	  = getColorLatex(ii);
                mar	  = getMarkerLatex(1);
                #leg = "\$ \\lambda \$ $(Int(perf1.burstRange[ii])) PPN N (K $(perf1.λRange[ib]))";
                #leg = "$(windoww[ii]) $(mode[ij]), \$\\lambda\$  $(perf1.λRange[ib])";
                #leg = "$(windoww[ii]), \$\\lambda\$  $(perf1.λRange[ib])";
                push!(plt, @pgf PGFPlotsX.Plot({color=col,mark="no mark", style=linestyle[ib]},Table([perf1.snrRange,temp4])));
                #push!(plt,@pgf LegendEntry(leg));
            end
        end
    end
    
        display(plt);
        pgfsave("plotsPPN.pdf", plt)	
    end
    
    
    
    
    function plotPerfPPN()
    
        # --- Figure
        @pgf plt = SemiLogYAxis({
                                height      ="3in",
                                width       ="4in",
                                grid, 
                                yminorgrids,
                                xminorgrids,
                                xlabel      = "PPN Overlapping factor (k)",
                                ylabel      = "Channel Error Rate",
                                title       = "Channel error rate (no modulation, no red signal, SNR 40 dB, N 80, burst dur 100 * N)",
                                #ymin		   = 1e-5,
                                legend_style="{at={(1,0)},anchor=south west,legend cell align=left,align=left,draw=white!15!black}"
                                },	);
    
    
    
    
        # Output1[delayRange,burstRange,hRange,snrRange,cfoRange,λRange,windowRange,overRange,sleepRange,algoRange,:]
    
        k = [2.0 4.0 8.0 10.0 12.0 16.0 30.0 40.0 50.0 60.0 70.0 80.0 90.0 100.0]
        #=	
        kaiser  =[[ ];[]]
        cosine  =[[ ];[]]
        gauss1  =[[ ];[]]
        gauss33 =[[ ];[]]
        gauss50 =[[ ];[]]
        gauss100=[[ 0.828003  0.861174  0.739232  0.615556  0.625607  0.568522  0.417764  0.477027  0.522824  0.615259  0.724428  0.794237  0.878503  0.948757];[ 0.828993  0.861719  0.742351  0.611051  0.626448  0.543173  0.409694  0.468363  0.514952  0.61823  0.709476  0.793742  0.859937  0.941034]]
    
    
    
        kaiser  =[[ ];[0.00990197  0.0295574  0.0682741  0.087781  0.107288  0.146302  0.284286  0.374592  0.482771  0.56669  0.67982  0.774433  0.86365  0.960442]]
        cosine  =[[ ];[0.306273  0.285066  0.246971  0.227203  0.205351  0.166213  0.0290826  0.0696582  0.163923  0.264302  0.363985  0.457656  0.552172  0.662336]]
        gauss1  =[[ ];[0.00980295  0.0292603  0.0686207  0.0882266  0.107832  0.147787  0.285721  0.380384  0.477919  0.575453  0.683236  0.766611  0.876869  0.965591]]
        gauss33 =[[ ];[0.00985246  0.0294089  0.0693138  0.0882266  0.108377  0.147044  0.285721  0.386177  0.470641  0.578374  0.672987  0.766611  0.872463  0.975394]]
        gauss50 =[[];[0.00980295  0.0292603  0.0689672  0.0868898  0.107832  0.147044  0.279978  0.372661  0.475493  0.56669  0.672987  0.766611  0.868056  0.980295]]
        gauss100=[[];[0.00985246  0.0294089  0.0693138  0.087781  0.107832  0.144074  0.285721  0.386177  0.473067  0.581295  0.672987  0.762699  0.868056  0.960689]]
    =#
        temp4 = Array{eltype(kaiser)}(undef, length(k))
    
    
        for ii = [1 2]		
            for ik = 1:1:length(k)
                temp4[ik]= kaiser[ii,ik];
            end	
            col	  = getColorLatex(1);
            mar	  = getMarkerLatex(ii);
            if ii == 1 leg = "Kaiser";
            else leg = "Kaiser fft";
            end
            push!(plt, @pgf PGFPlotsX.Plot({color=col,mark=mar},Table([k,temp4])));
            push!(plt,@pgf LegendEntry(leg));
        end
    
        for ii = [1 2]		
            for ik = 1:1:length(k)
                temp4[ik]= cosine[ii,ik];
            end	
            col	  = getColorLatex(2);
            mar	  = getMarkerLatex(ii);
            if ii == 1 leg = "Cosine";
            else leg = "Cosine fft";
            end
            push!(plt, @pgf PGFPlotsX.Plot({color=col,mark=mar},Table([k,temp4])));
            push!(plt,@pgf LegendEntry(leg));
        end
    
        for ii = [1 2]		
            for ik = 1:1:length(k)
                temp4[ik]= gauss1[ii,ik];
            end	
            col	  = getColorLatex(3);
            mar	  = getMarkerLatex(ii);
            if ii == 1 leg = "Gaussian 0.1";
            else leg = "Gaussian 0.1 fft";
            end
            push!(plt, @pgf PGFPlotsX.Plot({color=col,mark=mar},Table([k,temp4])));
            push!(plt,@pgf LegendEntry(leg));
        end
    
        for ii = [1 2]		
            for ik = 1:1:length(k)
                temp4[ik]= gauss33[ii,ik];
            end	
            col	  = getColorLatex(4);
            mar	  = getMarkerLatex(ii);
            if ii == 1 leg = "Gaussian 0.33";
            else leg = "Gaussian 0.33 fft";
            end
            push!(plt, @pgf PGFPlotsX.Plot({color=col,mark=mar},Table([k,temp4])));
            push!(plt,@pgf LegendEntry(leg));
        end
    
    
        for ii = [1 2]	
            for ik = 1:1:length(k)
                temp4[ik]= gauss50[ii,ik];
            end	
            col	  = getColorLatex(5);
            mar	  = getMarkerLatex(ii);
            if ii == 1 leg = "Gaussian 0.5";
            else leg = "Gaussian 0.5 fft";
            end
            push!(plt, @pgf PGFPlotsX.Plot({color=col,mark=mar},Table([k,temp4])));
            push!(plt,@pgf LegendEntry(leg));
        end
    
    
    
    
    
        display(plt);
        pgfsave("plots.pdf", plt)
        
    end
    
    
    function plotPerfWavelet2(perf1)
    
        # --- Figure
        @pgf plt = SemiLogYAxis({
            height      = "3in",
            width       = "4in",
            grid, 
            yminorgrids,
            xminorgrids,
            xlabel      = "SNR [dB]",
            ylabel      = "Channel Error Rate",
            #title       = "Channel error rate, GFSK, 3 tone (h 0.01), 250 ps async",
            title       = "Channel error rate, Wavelet chanalyzer detector, \$\\lambda\$ 60, no modulation", 
            #title       = "Channel error rate, GFSK, 3 tone (h 0.01), in sync",
            ymin		= 1e-3,	
            xmin 		= -35,
            xmax 		= 10,
            legend_style= "{at={(1.1,0)},anchor=south west,legend cell align=left,align=left,draw=white!15!black}"
            },	);
        # Output1[delayRange,burstRange,hRange,snrRange,cfoRange,λRange,windowRange,overRange,sleepRange,algoRange,:]
        
        temp1 = mean((perf1.Output1), dims=11)
        temp2 = mean((perf1.Output2), dims=11)
        
        temp1 .= max.(temp1, 9e-6)
        temp2 .= max.(temp2, 9e-6)
        linestyle = ["{solid}" "{dashed}"  "{dashed}" "{ultra thin},{dotted}"]
        
        mode = ["CWT" "DWT"]
        windoww = ["Savitzky Golay" "No filter" ]
        
        
        for ii = 1:1:length(perf1.λRange)
            leg = "K $(perf1.λRange[ii]) ";	
            col	  = getColorLatex(ii);
            mar	  = getMarkerLatex(1);
            push!(plt,@pgf PGFPlotsX.Plot({color=col,mark="no mark", style="{ultra thick},{solid}"},Table([ [-100.0] , [1e-5] ])));
            push!(plt,@pgf LegendEntry(leg));
        end
    
        for ij = [1 2]
            for ii = [1 2]
                leg = "$(mode[ii]) $(windoww[ij])";	
                col	  = getColorLatex(6);
                mar	  = getMarkerLatex(ii);
                push!(plt,@pgf PGFPlotsX.Plot({color=col,mark=mar, style=linestyle[ij]},Table([ [-100.0] , [1e-5] ])));
                push!(plt,@pgf LegendEntry(leg));
            end
        end
        
        
        for ib = 1:1:length(perf1.λRange)	
        #	ib = 1
            for ij = [1 2]#1:1:length(perf1.overRange)
            #ij = 3
                for ii = [1 2]#1:1:length(perf1.windowRange)#
                    temp4 = Array{eltype(temp1)}(undef, length(perf1.snrRange))
                    #print("$(perf1.snrRange[ii]) dB : $(mean(skipmissing(temp2[1,1,1,ii,:,:,:,:,:,:])))\n")
    
                    for ik = 1:1:length(perf1.snrRange)
                        temp4[ik]= temp1[1,1,1,ik,1,ib,ii,ij,1,1]; #temp1[sync,ii,1,ik,1,1,1,1,1,3];
                    end
                    col	  = getColorLatex(ib);
                    mar	  = getMarkerLatex(ii);
                    #leg = "\$ \\lambda \$ $(Int(perf1.burstRange[ii])) PPN N (K 32)";
                    #leg = "$(windoww[ii]) $(mode[ij]), \$\\lambda\$  $(perf1.λRange[ib])";
                    #leg = "$(mode[ii]) $(windoww[ij]) , K  $(perf1.λRange[ib])";
                    push!(plt, @pgf PGFPlotsX.Plot({color=col,mark=mar, style=linestyle[ij]},Table([perf1.snrRange,temp4])));#
                    #push!(plt,@pgf LegendEntry(leg));
                end
            end
        end
        
            display(plt);
            pgfsave("plotsWav.pdf", plt)	
        end
    
    
    function plotPerfWavelet()
    
        # --- Figure
        @pgf plt = SemiLogYAxis({
                                height      ="3in",
                                width       ="4in",
                                grid, 
                                yminorgrids,
                                xminorgrids,
                                xlabel      = "Overlapping factor (k)",
                                ylabel      = "Channel Error Rate",
                                title       = "Channel error rate (no modulation, no red signal, SNR -20 dB, N 80, burst dur 100 * N)",
                                #ymin		   = 1e-5,
                                legend_style="{at={(1,0)},anchor=south west,legend cell align=left,align=left,draw=white!15!black}"
                                },	);
    
    
    
    
        # Output1[delayRange,burstRange,hRange,snrRange,cfoRange,λRange,windowRange,overRange,sleepRange,algoRange,:]
    
        k = [2.0 4.0 8.0 16.0][:]
    
        #-20 db
        # classique psd[1] psd perso
        haar  =[[0.922835  0.881003  0.797933  0.643404];[ 0.979622  0.974995  0.97037  0.962142];[ 0.904808  0.860019  0.773676  0.627823]]
        morlet  =[[ 0.900296  0.83284  0.693116  0.472542];[ 0.976245  0.969586  0.960685  0.945294];[ 0.85958  0.794917  0.646352  0.433941 ]]
    
        #40 db
        #haar  =[[0.0111234  0.00498155  0.0199246  0.0590726];[0.338062  0.336467  0.385852  0.473552];[ 0.0088903  0.00475989  0.0182016  0.0566264 ]]
        #morlet  =[[ 0.00941469  0.00490604  0.019776  0.0588593 ];[ 0.0144515  0.00968159  0.00488177  0.0197758 ];[0.0126135  0.00927968  0.00480851  0.0189495]]
    
    
        temp4 = Array{eltype(haar)}(undef, length(k))
    
    
        for ii = [1 2 3]		
            for ik = 1:1:length(k)
                temp4[ik]= haar[ii,ik];
            end	
            col	  = getColorLatex(1);
            mar	  = getMarkerLatex(ii);
            if ii == 1 leg = "Haar DWT no filter";
            elseif ii == 2 leg = "Haar DWT filter SoA";
            else leg = "Haar DWT filter adjusted";
            end
            push!(plt, @pgf PGFPlotsX.Plot({color=col,mark=mar},Table([k,temp4])));
            push!(plt,@pgf LegendEntry(leg));
        end
    
        for ii = [1 2 3 ]		
            for ik = 1:1:length(k)
                temp4[ik]= morlet[ii,ik];
            end	
            col	  = getColorLatex(2);
            mar	  = getMarkerLatex(ii);
            if ii == 1 leg = "Morlet CWT no filter";
            elseif ii == 2 leg = "Morlet CWT filter SoA";
            else leg = "Morlet CWT filter adjusted";
            end
            push!(plt, @pgf PGFPlotsX.Plot({color=col,mark=mar},Table([k,temp4])));
            push!(plt,@pgf LegendEntry(leg));
        end
    
        
        display(plt);
        pgfsave("plots.pdf", plt)
        
    end

    function plotPerf2(perf1, perf2)

        # --- Figure
        layout = Plotly.Layout(;title="Channel error rate FFT decimation methode (no red signal)",
                               xaxis_title="SNR (dB)", #xaxis_title="Delay (in sample) - Burst of 800 samples",
                               yaxis_title="Channel Error Rate",
                               yaxis_type="log",
                               xaxis_showgrid=true, yaxis_showgrid=true,
                               # yaxis_range=[0, 100]
                               )
        # Output1[delayRange2,burstRange2,hRange2,snrRange2,cfoRange2,λRange2,windowRange2,overRange2,sleepRange2,algoRange2,:]
    
        temp1 = mean((perf1.Output1), dims=11)
        temp2 = mean((perf2.Output1), dims=11)
    
        test = PlotlyBase.GenericTrace{Dict{Symbol,Any}}[]
        
        pl1	  = Plotly.scatter(; x= (perf1.snrRange) ,y=temp1[1,1,1,:,1,1,1,1,1,1] , name="Only sin carrier");
        push!(test, pl1);
    
        pl1	  = Plotly.scatter(; x= (perf2.snrRange) ,y=temp2[1,1,1,:,1,1,1,1,1,1] , name="GFSK");
        push!(test, pl1);	
    
        plt = Plotly.plot(test,layout);
        display(plt);
    end