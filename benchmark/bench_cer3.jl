using Distributed
@everywhere MODULE = :(Cer_batch);

# @info "We are in the Julia part, ready to compile the function";
# @everywhere include("batch_startup.jl");

# @info "Now, we launch the main call. Ready !"
# eval(:($MODULE.launchAll()));



module Cer_batch

# ----------------------------------------------------
## --- Core module loading
# ----------------------------------------------------
using FFTW
using LinearAlgebra
using Statistics
using DSP
using JLD2
using Distributed
using Infiltrator
using LoopVectorization
using SimplePlot #add https://gitlab.inria.fr/clavaud/SimplePlot.jl
using Plotly
using Statistics
using PGFPlotsX
using Dates
using StatsBase
using PortAudio #add https://github.com/JuliaAudio/PortAudio.jl
#using ProgressMeter

include("structs.jl")
using .Structs

include("batchTx.jl")
using .BatchTx

include("algos.jl")
using .Algos

include("../PEAQ/PEAQ.jl")
using .PEAQ

# --- Other specific calls
import DigitalComm.addNoise
import DigitalComm.addNoise!
import DigitalComm.avgPower

using WAV

#using FileIO: load, save, loadstreaming, savestreaming
#import LibSndFile


#include("../tests/Audio.jl")
#using .Audio
global 	MYID = "CERNoMod"


function launchAll()
	ARCHI = occursin("/srv",Sys.BINDIR) ? "igrida" : "pc"
	@show nbCore = nprocs()
	@show availableCore = Sys.CPU_THREADS
	@show  ARCHI
	@show MYID

	audioRate	  = Float32(48000)#8e3);	  # Audio rate
	sampleRate	  = Int32(80e6);	  # Total sampleRate
	# --- Time parameters#
	totalDuration = Float32(0.005);			  # Total duration [second] 
	oneTone = Int32(1021); 				# Frequency of one tone sin
	# --- blackMat parameters
	modeTx		= "gfsk";	  # blackMat type ("ones","rand","bpsk", "qpsk")
	modeFH		= "rand";	  # Frequency hoping jump type: ("rand","linear","UMTS")

	λRange 		= [624 20]#[6 20]#[6 20]# 20 200]; #Duration of carrier occupation before jump [nbChannels/sampleRate]
	hRange	  	= [0.00 0.01 0.1 0.25 0.5]#[0.5 0.25 0.10 0.01] #exp10.(range(0.0001, stop=1, length=50))./10 #index modulation
	snrRange	= -50:1:0;# [-6 -4 -2 0 20]
	cfoRange	= [0]#[10 15 20 25 50]#-20:4:0; 30 40 50 60 70 90 100 125 150 175 200 300 400 500 600 700 800 900 1000
	delayRange	= [0]#[0 3 5 10 15 20 30 50 70 90 123 155 200 220 270 300 333 350 380]
	KRange		= [20]#[2 4 8 16]#[2.0 4.0 8.0 10.0 12.0 16.0 30.0 40.0 50.0 60.0 70.0 80.0 90.0 100.0]
	NRange		= [40]#[1 2 3]#[1 2 3 4 5]#[1 2]#[1 2 4 8 16 32]
	sleepOffRange= [0]#[1 2]#[0 1 2]#[1 2 3]#[1 3 5]
	sleepOnRange = [1]#
	algoRange   = [1 2 3 4]
	nbIt		= Int32(250);

	λRange		=collect(Int32, λRange[:])
	hRange		=collect(Float32, hRange[:])
	snrRange	=collect(Float32, snrRange[:])
	cfoRange	=collect(Int32,   cfoRange[:])
	KRange		=collect(Float32, KRange[:])
	delayRange	=collect(Int32,   delayRange[:])
	NRange		=collect(Int32,   NRange[:])
	sleepOffRange=collect(Int32,  sleepOffRange[:])
	sleepOnRange=collect(Int32,   sleepOnRange[:])
	algoRange	=collect(Int32,   algoRange[:])



	CER			= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	SBOS			= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	ODG				= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	BandwidthRef 	= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	BandwidthTest	= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	totalNMR		= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	WinModDiff1		= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	ADB				= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	EHS				= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	AvgModDiff1		= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	AvgModDiff2		= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	RmsNoiseLoud	= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	MFPD			= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	RelDistFrames	= zeros(Union{Float32},length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);

	tS = [];	
	for iDelay = 1 : length(delayRange)
		currentDelay=delayRange[iDelay];
		for iλ = 1 : length(λRange)
			currentλ=λRange[iλ];
			for ih = 1 : length(hRange)
				currentH=hRange[ih];
				 for iSnr = 1 : length(snrRange)
					currentSNR=snrRange[iSnr];
					for iCfo = 1 : length(cfoRange)
						currentCfo=cfoRange[iCfo];
						for iK = 1 : length(KRange)
							currentK=KRange[iK];
							for iN = 1 : length(NRange)
								currentN=NRange[iN];
								nbChannels	= Int32(currentN);			  # Number of channels (to have 200kHz channel)
								channelBW	= Float32(sampleRate/nbChannels) #800e3;		  # sampleRate of one channel
								blackRate  	= Float32(channelBW/2);   # Targeting a 100 kHz usefull signal sampleRate
								paramSimul 	= ParamSimul(
									audioRate,
									sampleRate,
									nbChannels,
									channelBW,
									blackRate,
									totalDuration,
									oneTone,
									modeTx,
									modeFH,
									ARCHI); 
								
								for iSleepOff = 1 : length(sleepOffRange)
									currentSleepOff=sleepOffRange[iSleepOff];
									for iSleepOn = 1 : length(sleepOnRange)
										currentSleepOn=sleepOnRange[iSleepOn];
										for iAlgo = 1 : length(algoRange)
											currentAlgo=algoRange[iAlgo];
											cS	= @spawnat :any Cer_batch.sim(paramSimul,[currentλ],[currentH],[currentSNR],[currentCfo],[currentK],[currentDelay],[currentN],[currentSleepOff],[currentSleepOn],[currentAlgo],nbIt);
											push!(tS,cS);
										end #end iAlgo
									end #end iSleepOn
								end #end iSleepOff
							end #end iN
						end #end iK
					end #end iCfo								
				end #end iSnr
			end #end ih
		end #end iDelay
	end #end iλ
	
			
					println("$(MYID) Simulation dispatched with $(length(tS)) jobs pending")		
					flush(stdout)
					foreach(wait,tS);					
                    println("$(MYID) wait done")	
					flush(stdout)
					cnt = 0									
					#fetch(tS)
					# @infiltrate
					# @sync begin
					# 	@async begin
							for ii = 1:length(tS)
                                flush(stdout)
								perfRet = fetch(tS[ii])
								delayRange2 = findfirst(x->x==perfRet.delayRange[1],delayRange[:])
								λRange2 = findfirst(x->x==perfRet.λRange[1],λRange[:])
								hRange2 = findfirst(x->x==perfRet.hRange[1],hRange[:])
								snrRange2 = findfirst(x->x==perfRet.snrRange[1],snrRange[:])
								cfoRange2 = findfirst(x->x==perfRet.cfoRange[1],cfoRange[:])
								KRange2 = findfirst(x->x==perfRet.KRange[1],KRange[:])
								NRange2 = findfirst(x->x==perfRet.NRange[1],NRange[:])
								sleepOffRange2 = findfirst(x->x==perfRet.sleepOffRange[1],sleepOffRange[:])
								sleepOnRange2 = findfirst(x->x==perfRet.sleepOnRange[1],sleepOnRange[:])
								algoRange2 = findfirst(x->x==perfRet.algoRange[1],algoRange[:])
		
								CER[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.CER[:]
								SBOS[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.SBOS[:]
								ODG[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.ODG[:]
								BandwidthRef[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.BandwidthRef[:]
								BandwidthTest[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.BandwidthTest[:]
								totalNMR[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.totalNMR[:]
								WinModDiff1[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.WinModDiff1[:]
								ADB[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.ADB[:]
								EHS[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.EHS[:]
								AvgModDiff1[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.AvgModDiff1[:]
								AvgModDiff2[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.AvgModDiff2[:]
								RmsNoiseLoud[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.RmsNoiseLoud[:]
								MFPD[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.MFPD[:]
								RelDistFrames[delayRange2,λRange2,hRange2,snrRange2,cfoRange2,KRange2,NRange2,sleepOffRange2,sleepOnRange2,algoRange2,:] = perfRet.RelDistFrames[:]
							end

							tS = []
							GC.gc()		
	
	perf = PerfSave(
		audioRate,
		sampleRate,
		0.0f0,
		0.0f0,
		0.0f0,
		λRange,
		totalDuration,
		0,
		modeTx,
		modeFH,
		hRange,
		snrRange,
		cfoRange,
		delayRange,
		KRange,
		NRange,
		sleepOffRange,
		sleepOnRange,
		algoRange,
		nbIt,
		[],
		[],
		CER,
		SBOS,
		ODG,
		BandwidthRef,
		BandwidthTest,
		totalNMR,
		WinModDiff1,
		ADB,
		EHS,
		AvgModDiff1,
		AvgModDiff2,
		RmsNoiseLoud,
		MFPD,
		RelDistFrames,
		[],
		[]
		);

	#@show mean(perf.SBOS[:])
 	datetimenow = Dates.now()
 	ttt = Dates.format(datetimenow, "_mm_dd__HH_MM")

		
 	if ARCHI == "pc"
		rr = pwd()
		@save "$rr/perfModNo"*ttt*".jld2" perf		
	else   	
      #  try
       # 	scratch = "/udd/clavaud/Documents/tempestrealtime.jl/benchmark"
      #      @save "$scratch/perf.jld2" perf		
      #      @info scratch
      #  catch
     #   	@warn "failed save $scratch"
     #   end
        
        try
        	scratch = "/tmp"
            @save "$scratch/perf$(MYID).jld2" perf		
            @info MYID
            @info scratch
            mv("$scratch/perf$(MYID).jld2", "/udd/clavaud/Documents/tempestrealtime.jl/benchmark/perf$(MYID)_$(ttt).jld2")
        catch
        	@warn "$(MYID) failed save $scratch"
        end                
        
        # try
        #    rr = "/temp_dd/igrida-fs1/clavaud/SCRATCH"
        #    @save "$rr/perf"*ttt*".jld2" perf		
        #    @info rr
      #  catch
       # 	@warn "failed save $rr"
     #   end       
	end
	
 rmprocs(2:999)

	if ARCHI == "pc"
		return perf
	else	
		return 0 ##0		
	end
end



function skipNaN(x)
	len = length(x)
	out = []
	for ii = 1:len
		if isnan(x[ii])==false
			push!(out,x[ii])
		end
	end
	if length(out)==0
		return NaN
	else
		return out
	end

end

function evaluateCer(p, p̂, nbChannels, currentAlgo, currentK,currentλ)
	occ = 0#sum( Int.( p .!= p̂))
	len2 = 0;

	len = min(length(p̂),length(p));

	#temp = crosscor(p̂[1:len], p[1:len], [-200:200]...)
	#@info argmax(temp)-200
	#SimplePlot.plot2(temp.-200,"cross")
	#@infiltrate	
	for ii in 1:len
		len2 +=1;
		if p[ii] != p̂[ii]
			occ+=1;
		end
	end	
	return occ / len2
end






# ----------------------------------------------------
## --- Main call
# ----------------------------------------------------Int32
function sim(para::ParamSimul, λRange,hRange,snrRange,cfoRange,KRange,delayRange,NRange,sleepOffRange,sleepOnRange,algoRange,nbIt)
	audioRate	  = Float32(para.audioRate)
	sampleRate	  = Int32(para.sampleRate);
	nbChannels	  = Int32(para.nbChannels);
	channelBW	  = Float32(para.channelBW)
	blackRate  	  = Float32(para.blackRate);
	totalDuration = Float32(para.totalDuration);

	nbIt		= Int32(nbIt)
	λRange		=collect(Float32, λRange[:])
	hRange		=collect(Float32, hRange[:])
	snrRange	=collect(Float32, snrRange[:])
	cfoRange	=collect(Int32,   cfoRange[:])
	KRange		=collect(Float32, KRange[:])
	delayRange	=collect(Int32,   delayRange[:])
	NRange		=collect(Int32,   NRange[:])
	sleepOffRange=collect(Int32,   sleepOffRange[:])
	sleepOnRange=collect(Int32,   sleepOnRange[:])
	algoRange	=collect(Int32,   algoRange[:])

	
	# p = Progress(length(λRange)*length(hRange)*length(snrRange)*length(cfoRange)*length(KRange)*length(delayRange)*length(NRange)*length(sleepOffRange)*length(sleepOnRange)*length(algoRange)*nbIt,
	# dt=0.5, color=:blue, desc="Computing...",barglyphs=BarGlyphs('|','█', ['▁' ,'▂' ,'▃' ,'▄' ,'▅' ,'▆', '▇'],' ','|',),
    # barlen=40) #progression meter

	# ----------------------------------------------------
	# --- Init vectors
	# ----------------------------------------------------
	CER					= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	SBOS				= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	ODG					= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	BandwidthRef		= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	BandwidthTest		= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	totalNMR			= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	WinModDiff1			= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	ADB					= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	EHS					= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	AvgModDiff1			= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	AvgModDiff2			= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	RmsNoiseLoud		= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	MFPD				= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);
	RelDistFrames		= zeros(Float32,length(delayRange),length(λRange),length(hRange),length(snrRange),length(cfoRange),length(KRange),length(NRange),length(sleepOffRange),length(sleepOnRange),length(algoRange),nbIt);

#PEAQ.loadPEAQ()	
	rr = pwd()
	rrPeaq = "$rr/../PEAQ"
	pa = "$rrPeaq/guitar48_stereo_1_sec.wav"
	y, fs, nbits, opt = wavread(pa)
	y = nothing
	
	 if para.ARCHI == "pc"
		scratch = pwd()
	else
		scratch = "/temp_dd/igrida-fs1/clavaud/SCRATCH"
	end
	
	

	for iDelay = 1 : length(delayRange)
		currentDelay=delayRange[iDelay];		
		# --- Iteration in duration
		for iλ = 1 : length(λRange)
			currentλ=λRange[iλ];			
			#sigRx		  = Array{Complex{Float32}}(undef, Int(ceil(sampleRate*totalDuration ))) # TODO add currentDelay
			#sigTx		  = Array{Complex{Float32}}(undef, Int(ceil(sampleRate*totalDuration)))
			for ih = 1 : length(hRange)
				currentH=hRange[ih];
				for iSnr = 1 : length(snrRange)
					currentSNR=snrRange[iSnr];
					for iCfo = 1 : length(cfoRange)
						currentCfo=cfoRange[iCfo];
						for iK = 1 : length(KRange)
							#currentK=KRange[iK];
							currentK=currentλ
							#currentK=Int(currentλ); #!!!!!!!!
							for iN = 1 : length(NRange)
								currentN=NRange[iN];
								if currentλ == 624
                                    currentN = 40
                                else
                                    currentN = 512
                                end							
								
								for iSleepOff = 1 : length(sleepOffRange)
									currentSleepOff=sleepOffRange[iSleepOff];
									for iSleepOn = 1 : length(sleepOnRange)								
										currentSleepOn=sleepOnRange[iSleepOn];
										#println("$(myid()) -  start TX")	
										sigTx, p, OrAudioContent = BatchTx.txGFSK(totalDuration,	
										channelBW,	
										sampleRate,
										nbChannels,
										currentλ,
										currentSleepOn,
										currentSleepOff,																
										currentH,
										audioRate);
								
										#println("$(myid()) -  done TX")
										rep = 5

										
										#wavwrite(resample(OrAudioContent,(48000 / audioRate)), "ref_$(myid()).wav", Fs=48000)
										###wavwrite(BatchTx.convert(repeat(OrAudioContent,rep)[1:Int(audioRate*totalDuration*(rep-1))]), "$scratch/ref_$(myid()).wav", Fs=audioRate, nbits=16, compression=WAVE_FORMAT_PCM, chunks=opt)
										#SimplePlot.plotSpectrum(audioRate,OrAudioContent)
										
										#save("ref_$(myid()).wav", OrAudioContent) 

										for iAlgo = 1 : length(algoRange)
											currentAlgo=algoRange[iAlgo];
											
											# ----------------------------------------------------
											# --- Monte carlo
											# ----------------------------------------------------
											#for iNbit = 1 : ((currentAlgo == 4) ? 1 : nbIt)	
											for iNbit = 1 : nbIt		 
												#println("$(myid()) -  start TX2")		
																		
												sigTx, p, = BatchTx.txRed(totalDuration,	
												#sigTx, p, OrAudioContent = BatchTx.txGFSK(totalDuration,	
																channelBW,	
																sampleRate,
																nbChannels,
																currentλ,
																currentSleepOn,	
																currentSleepOff,															
																currentH,
																audioRate);
												#println("$(myid()) -  done TX2")										
												sigRx		  = Array{Complex{Float32}}(undef, Int(length(sigTx)-currentDelay))												
												#sigRx = sigTx
												addNoise!(sigRx, sigTx[1+currentDelay : end],currentSNR);	 
												sigTx = []
												p2 = vcat(fill.(p, Int(nbChannels*currentλ))...)[1+currentDelay : end]
											
												if currentAlgo == 1
													audioContent,p̂,brownData,allPower = Algos.Rotor(sigRx, para, currentK); #Algos.Rotor(sigRx, para, currentλ); 													
													p̂2 = vcat(fill.(p̂, Int(nbChannels*currentK ))...)
												elseif currentAlgo == 2
													audioContent,p̂,brownData,allPower = Algos.Fft(sigRx, para, currentK);													
													p̂2 = vcat(fill.(p̂, Int(1024/1))...)
													#p̂2 = vcat(fill.(p̂, Int(1024/1))...)
												elseif currentAlgo == 3
													audioContent,p̂,brownData,allPower = Algos.PPN2(sigRx, para, currentK,3,2);
													p̂2 = vcat(fill.(p̂, Int(nbChannels*currentK))...)	
													#p̂2 = p̂2[Int((nbChannels*1)*(2-1))+1:end] #1024 -10	
													#@infiltrate
													#p̂2 = vcat(fill.(p̂, 1024)...)																												
												else                                                                  
													audioContent,p̂,brownData,allPower = Algos.wavelett(sigRx, para, currentK, 3, 1);
													#p̂ = p̂[Int(currentK/2):end]	
													p̂2 = vcat(fill.(p̂[Int(currentK/2):end], Int(nbChannels))...)	
													#p̂2 = p̂2[Int((nbChannels)*currentK/2+1):end]													
												end

												#SimplePlot.plot2(p̂, "p̂")
												#SimplePlot.plot2( p, "p")
												
												#SimplePlot.plotList([p2[1:1:1000000], p̂2[1:1:1000000], p2[1:1:1000000]-p̂2[1:1:1000000]], ["p", "p̂","p-p̂"])
												#SimplePlot.plotList([p2[1:100000], p̂2[1:100000], p2[1:100000]-p̂2[1:100000]], ["p", "p̂","p-p̂"])
												mmlen = min(length(p̂2),length(p2));
												#SimplePlot.plotList([p2[1:mmlen], p̂2[1:mmlen], p2[1:mmlen]-p̂2[1:mmlen]], ["p", "p̂","p-p̂"])
												cer = evaluateCer(p2, p̂2, nbChannels, currentAlgo, currentK,currentλ)

												#audioContent = repeat(audioContent,10+1)

												#wavwrite(resample(audioContent,(48000 / audioRate)), "test_$(myid()).wav", Fs=48000)
												#wavwrite(audioContent, "test_$(myid()).wav", Fs=audioRate, nbits=32, compression=WAVE_FORMAT_IEEE_FLOAT, chunks=opt)
												###wavwrite(BatchTx.convert(repeat(audioContent,rep)[1:Int(audioRate*totalDuration*(rep-1))]), "$scratch/test_$(myid()).wav", Fs=audioRate, nbits=16, compression=WAVE_FORMAT_PCM, chunks=opt)
												#odg = PEAQ.evalPEAQ("../benchmark/ref_$(myid()).wav","../benchmark/test_$(myid()).wav")
												
												#play44(audioContent,audioRate)
												#audioContent = audioContent#.-mean(audioContent)
												#SimplePlot.plotSpectrum(audioRate,audioContent)
										
												#if ismissing(timeResync) == true # prevent using of sparse matrix
												#	CER[iDelay,iλ,ih,iSnr,iCfo,iK,iN,iSleepOff,iSleepOn,iAlgo,iN] = NaN32
												#	SBOS[iDelay,iλ,ih,iSnr,iCfo,iK,iN,iSleepOff,iSleepOn,iAlgo,iN]  = NaN32
												#else				
													
													CER[iDelay,iλ,ih,iSnr,iCfo,iK,iN,iSleepOff,iSleepOn,iAlgo,iNbit]  		= cer
													SBOS[iDelay,iλ,ih,iSnr,iCfo,iK,iN,iSleepOff,iSleepOn,iAlgo,iNbit]  		= 0f0#PEAQ.visqol("$scratch/ref_$(myid())","$scratch/test_$(myid())")
												
			
												#end
												 #if iNbit%10==9
												 #    println("$(myid()) -  $(iNbit)")
												 #end
												#next!(p; showvalues = [(:currentDelay,currentDelay),(:currentλ,currentλ),(:currentH,currentH),(:currentSNR,currentSNR),(:currentCfo,currentCfo),(:currentK,currentK),(:currentN,currentN),(:currentSleepOff,currentSleepOff),(:currentSleepOn,currentSleepOn),(:currentAlgo,currentAlgo),(:iN,iN),(:metric,SBOS[iDelay,iλ,ih,iSnr,iCfo,iK,iN,iSleepOff,iSleepOn,iAlgo,iN])])
											end #end iN 
										end #end iAlgo
									end #end iSleepOn
								end #end iSleepOff
							end #end iN
						end #end iK
					end #end iCfo
				end #end iSnr
			end #end ih
		end #end iλ
	end #end iDelay
	
	audioContent = nothing
	OrAudioContent = nothing
	sigRx = nothing
	GC.gc()	

	
	perf = PerfSave(
		audioRate,
		sampleRate,
		0.0f0,
		0.0f0,
		0.0f0,
		λRange,
		totalDuration,
		0,
		"nope",
		"nope",
		hRange,
		snrRange,
		cfoRange,
		delayRange,
		KRange,
		NRange,
		sleepOffRange,
		sleepOnRange,
		algoRange,
		nbIt,
		[],
		[],
		CER,
		SBOS,
		ODG,
		BandwidthRef,
		BandwidthTest,
		totalNMR,
		WinModDiff1,
		ADB,
		EHS,
		AvgModDiff1,
		AvgModDiff2,
		RmsNoiseLoud,
		MFPD,
		RelDistFrames,
		[],
		[]
		);

	datetimenow = Dates.now()
	ttt = Dates.format(datetimenow, "_mm_dd__HH_MM")
	println("$(myid()) -  $(MYID) - Perf sent"*ttt)
	flush(stdout)
	
	
	# --- Return tuple of performance objects
	return (perf)
end


end #end module
