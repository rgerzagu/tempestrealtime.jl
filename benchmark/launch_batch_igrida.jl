# be sure that julia is launched as followd
# julia -p N simulations/ChannelOnlyCER/launch_igrida.jl
# Number of core are in batch.sh no need to redoing stuff here
#  oarsub -S $HOME/Documents/tempest_fh/batch.sh

#@show ENV["SCRATCHDIR"]

# --- Define MODULE of interest
@everywhere using Distributed
#addprocs(7)#[(remote_machine_spec, 7)], tunnel=true)
#@everywhere using Distributed

#@everywhere using Revise
@everywhere MODULE = :(Cer_batch);

@everywhere cd("/udd/clavaud/Documents/cer_bench/")
@everywhere @info "We are in the Julia part, ready to compile the function";

@everywhere include("batch_startup.jl");
@everywhere include("batch_startup.jl");
@everywhere include("batch_startup.jl");

@everywhere include("bench_cer.jl")
@everywhere include("bench_cer.jl")

@info "Now, we launch the main call. Ready !"
eval(:($MODULE.launchAll()));


# --- result should be on
# julia> ENV["SCRATCHDIR"]
# "/temp_dd/igrida-fs1/$USER/SCRATCH"
