module Test

# ----------------------------------------------------
## --- Core module loading
# ----------------------------------------------------
using FFTW
using LinearAlgebra
using Statistics
using DSP
using JLD2
using Distributed
using Infiltrator
using LoopVectorization
using SimplePlot
using Plotly
using Statistics
using PGFPlotsX
using Dates
using BenchmarkTools
#using ProgressMeter

include("structs.jl")
using .Structs

include("batchTx.jl")
using .BatchTx

include("algos.jl")
using .Algos

# --- Other specific calls
import DigitalComm.addNoise
import DigitalComm.addNoise!
import DigitalComm.avgPower


using Wavelets
using Memoize
using ProfileView
#using cwt #add https://github.com/Ralf3/cwt



function savitzkyGolay(x::Vector, windowSize::Int, polyOrder::Int; deriv::Int=0)  
    isodd(windowSize) || throw("Window size must be an odd integer.")
    polyOrder < windowSize || throw("Polynomial order must me less than window size.")
    
    halfWindow = Int( ceil((windowSize-1)/2) )
    
    # Setup the S matrix of basis vectors
    S = zeros.(windowSize, polyOrder+1)
    for ct = 0:polyOrder
      S[:,ct+1] = (-halfWindow:halfWindow).^(ct)
    end
    
    ## Compute the filter coefficients for all orders
    G = S * pinv(S' * S)
    
    # Slice out the derivative order we want
    filterCoeffs = G[:, deriv+1] * factorial(deriv)
    
    # Pad the signal with the endpoints and convolve with filter
    paddedX = [x[1]*ones(halfWindow); x; x[end]*ones(halfWindow)]
    y = conv(filterCoeffs[end:-1:1], paddedX)
    
    # Return the valid midsection
    return y[2*halfWindow+1:end-2*halfWindow]
    
  end


  const TINY = nextfloat(0.0)
 
  
  # Complex Morlet. Real part (Time domain) *
  function CMORLETreal(x::Float32, a::Float32, b::Float32)
      Fb = 2.0 # bandwidth parameter 
      Fc = 0.8 # wavelet center frequency 
      c = 1.0 / sqrt(pi*Fb)
      a= a==0.0 ? TINY : a
      x = (x - b) / a
      x2 = x * x
      return c * exp(-x2/Fb) * cos(2*pi*Fc*x)
      end

  

  # dict for all defined wavelets
  
  struct Wavelet
      esl
      esh
      f 
  end
  
  global D=Dict()
  D["CMorlet"]=Wavelet(-4.0,4.0,CMORLETreal)
 
  
  
  # main function to produce an image of the wavlet transformation
  # inputs: wname, x
  #
  # parameters:
  #   width of the resulting image in pixels
  #   height of the resulting image in pixels
  #   check:
  #       width<=length(x) && hight<=length(x)
  #
  # wavelet parameters in pixels:
  #   a = 1   start with 1 as default
  #  da = floor(length(x)/hight) a= a==0 ? 1 : a
  #  da = parameter
  #   b = 0   start with no shift
  #  db = floor(length(x)/width) b= b==0 ? 1 : b
  
    
   function cwtf(wname::String,
               x::Array{Float32,1},
               height::Int64,width::Int64,
               a0=1.0f0, da=0f0)
      # perform all checks
      if haskey(D,wname) == false
          return NaN
      end
      l::Int64=length(x)
      if width>l || height>l
          return NaN
      end
      # calculate the paramters for iteration
      if da==0.0
          da= Float32(floor(l/height))
          da = da==0.0f0 ? 1.0f0 : da
      end
      db = floor(l/width)
      db = db==0.0f0 ? 1.0f0 : db
      b0=0.0f0
      #println("da=",da," db=",db)
      f=D[wname].f
      # define the output array and fill it with zero
      A = zeros(height,width)
      # main loop
      a=a0
      l3=3*l
      l2=Int(l3/2)
      FX=zeros(l3)                   # define an array for convolution
      for i::Int32=1:height          # loop for a
          b=b0
          for k1::Float32=1.0:l3 # fill F
                  FX[Int(k1)]=f(k1,a,l3/2.0f0)
          end
          for j::Int32=1:width      # loop for b
              for k::Int32=1:l  # Perform convolution 
                  A[i,j] += FX[Int(l2-b+k)]*x[k]
              end
              A[i,j] /= sqrt(a)
              b += db
           end
          a += da
      end
      return A
  end



  @memoize function cwtMorlet!(xt::Array{Float32,2}, x::Array{Float32,2}, height::Int64,width::Int64) 
      l::Int64=length(x)

      #Complex MORLET parameter
      Fb = 2.0 # bandwidth parameter 
      Fc = 0.8 # wavelet center frequency 
      c = 1.0 / sqrt(pi*Fb)

      # calculate the paramters for iteration
      db = Float32(floor(l/width) )    
      b0=0.0f0

      # main loop
      a=1.0f0
      l3=Int(3*l)
      l2=Int(l3/2)
      FX=zeros(l3)                   # define an array for convolution
      @inbounds for i=1:height          # loop for a
          b=b0
          @inbounds for k1=1:l3 # fill F
                  FX[k1]=c * exp(-(((k1 - l3/2) / a)^2)/Fb) * cos(2*pi*Fc*k1)
          end
          @inbounds for j=1:width      # loop for b
            @inbounds for k=1:l  # Perform convolution 
                xt[i,j] += FX[Int(l2-b+k)]*x[k]
              end
              xt[i,j] /= sqrt(a)
              b += db
           end
          a += 2.0f0
      end
  end


    nbChannel = 80
   
#Create test signal
    #frq=[-5.41f0 -2.2f0 1f0 3.1f0 5.3f0];
    frq=[-20f0];
    testInput=zeros(Complex{Float32},20000)
    testInputAWGN=zeros(Complex{Float32},20000)
    t = collect(Float32, 0:length(testInput)-1)    
    for k=1:length(frq)        
        testInput .= testInput .+ rand(1).*exp.((0.0f0+2im)*pi*frq[k]/nbChannel.*t); #+(0.0f0+2im)*pi*rand(1)
    end
    addNoise!(testInputAWGN, testInput,-20);	


   # SimplePlot.plotSpectrum(1,testInput)


#Create filter bank
   
    #SimplePlot.plot3(-(4-1/nbChannel):1/nbChannel:(4-1/nbChannel),h,"Impulse Response, $(filtLen-1)-Tap Prototype Nyquist Filter <br> $(nbChannel)-channel, $(nbTap)-Taps per channel")   


    

#temp var
   
    descentVectorC  = Vector{Complex{Cfloat}}(undef,0);
    allPower        = Vector{Cfloat}(undef,0);
    p̂               = Vector{Cfloat}(undef,0)

    averagingFac    = 16
    inWav           = zeros(Float32,nbChannel,averagingFac);
    det             = zeros(Float32,nbChannel,averagingFac); 
    xt              = zeros(Float32,nbChannel,averagingFac); 

    sigRX = testInputAWGN
    wt = wavelet(WT.haar)
    len2 = [0]
    occ = [0]
#PPN    
    for n=1:nbChannel:length(testInput)-nbChannel-1            
       sig = sigRX[n:n+nbChannel-1]

        psd = abs2.(fftshift(fft(sig)))
        #psdFilt = savitzkyGolay(psd, Int(round(nbChannel*0.3+1)), 4) 
        psdFilt = savitzkyGolay(psd, Int(round(nbChannel*0.05+1)), 4) 
        inWav[:]=[psdFilt inWav[:,1:averagingFac-1]];  
        #xt = dwt(inWav, wt)
        cwtMorlet!(xt, inWav, nbChannel,averagingFac)
        #xt = cwtf("Haar", inWav[:], nbChannel,averagingFac, 1.0f0,1.0f0)
       
        det .= xt-inWav
        test = sum(det,dims = 2)[:]      
        #SimplePlot.plotList([psd, psdFilt,test],["psd", "filtered", "output"])
        p̂ = argmin(test)
        #@info p̂
        if p̂ != 21
            occ[1] = occ[1]+1;
        end
        len2[1] = len2[1]+1;
       
       #push!(descentVector,abs2.(PPNOut[maxPos,:])...);

    end

    @info occ/len2
    
    #SimplePlot.plot2(abs2.(PPNOut[1,nbChannel:end]),"1")
    #SimplePlot.plot2(abs2.(PPNOut[2,nbChannel:end]),"2")
    #SimplePlot.plot2(abs2.(PPNOut[11,nbChannel:end]),"11")
    #SimplePlot.plot2(abs2.(PPNOut[15,nbChannel:end]),"15")
   
   # SimplePlot.plot2(p̂,"p̂")




    #outputFilt      = conv(descentVector,h2);
    #audioContent    = real.(outputFilt[1:secondDecim:end]);
    #audioContent    = normalizePower!(audioContent);


   # audioContent,p̂,descentVector,allPower

    #A generic function that I use everywhere to coerce a vector dim 0 to a row vector...
    forceMatrix( a ) = ( length( size( a ) ) == 1 ) ? reshape( a, length(a), 1 ) : a
    forceMatrixT( a ) = ( length( size( a ) ) == 1 ) ? reshape( a, 1, length(a) ) : a
    export forceMatrix, forceMatrixT


    """
    ConvFilter1DFFT(a, filter)
    Performs a 1D convolution of vector `filter` onto `a` via the FFT definition of a
    convolution. This method implicitly zeropads and truncates the result.
    Note: This isn't highly optimized for performance. It was made when DSP.jl broke,
    and broke the ChemometricsTools.jl package.
    """
    function ConvFilter1DFFT(a, filter)
        @assert(length(filter) <= length(a), "Length of filter should be less than the length of the vector." )
        NewSize = length(a) + length(filter) - 1;
        ADiff = Int(round((NewSize - length(a)) ))
        ADiffhalf = Int(floor(ADiff / 2))
        filterDiff = Int(round((NewSize - length(filter)) ))
        X = fft( vcat( a, zeros( ADiff ) ) )
        H = fft( vcat( filter, zeros( filterDiff ) ) )
        return real.(ifft( X .* H ))[ (ADiffhalf + 1):(end-ADiffhalf) ]
    end

    

      #=

   function SavitzkyGolay(X, DerivOrder, PolyOrder, windowsize::Int)
    @assert( (windowsize % 2) == 1, "Window size must be an odd number" )
    @assert( PolyOrder < (windowsize-1), "Polynomial order must be less than the window size." )
    X = forceMatrixT(X)
    (Obs,Vars) = size(X)
    windowspan = (windowsize - 1) / 2
    basis = ones( windowsize, PolyOrder+1 )
    basis[:,2:end] = ( (-windowspan:windowspan)' .^ ( 1 : PolyOrder ))'
    DerivFac = factorial( DerivOrder )
    FIR = inv( basis' * basis) * basis'
    output = DerivFac * ConvFilter1DFFT(X[1,:], FIR[DerivOrder+1,: ])'
    for r in 2:Obs
        sg = DerivFac * ConvFilter1DFFT(X[r,:], FIR[DerivOrder+1,: ])'
        output = vcat( output, sg )
    end
    offset = Int( windowspan )
    return output[:, (offset + 1) : (end - offset)]
end
=#

end