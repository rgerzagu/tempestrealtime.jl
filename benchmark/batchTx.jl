module BatchTx

using FFTW
using DSP
using JLD2
using Infiltrator
using LoopVectorization
using StaticArrays
using DigitalComm
using SimplePlot
#using Memoize

#include("../tests/Audio.jl")

function to44(sig,fe)
	rate = (44100 / fe);
	return resample(sig,rate);
end




function gaussDesign(BT,L,ovS)
    # --- Setting α from BT
    α   =  sqrt(log(2)/2)/(BT);
    # --- Time span
    t   = -L*ovS:1:L*ovS;
    # --- Filter
    h   = sqrt(pi)/α  *exp.(-(pi/ α.* t).^2);
    return h;
end


function testdata()
    duration = Float32(0.001)    
    samplingRate = Int32(80e6)
    N = Int32(80)
    burstMult = 20
    sleepMultOn = 3
    sleepMultOff = 1
    k = 0.0
    audioRate = Float32(48000)
    channelBW = Float32(samplingRate/N)

    d,sequence, audio = txGFSK(duration,channelBW,samplingRate,N,burstMult,sleepMultOn, sleepMultOff,k, audioRate)
    SimplePlot.plotList([real.(d)], ["d"])
    SimplePlot.plotSpectrum(samplingRate,d)

    #d = nothing
    return sequence
end

function convert(x)
    return Int16.(min.((round.(x.*2^12)),(2^15)-1))
end

function txGFSK(duration,channelBW,samplingRate,N,burstMult,sleepMultOn, sleepMultOff,k, audioRate)
    # ----------------------------------------------------
    # --- Parameters
    # ----------------------------------------------------
    #duration    = 0.2;                      # Duration in [s]
    #blackRate   = 1e6;                      # Data rate
    freqTones   = [250;450;750];       # Tone injection frequency [Hz] 250 450 750Hz 700;900;1200
    #samplingRate= 80e6;                     # Full rate
    #N           = 80;                       # Number of channels
    #λ           = 345;	                    # FH repetition factor
    #k           = 0.5;                        # Intermodulation coefficient
    g           = 1-k;                        # Direct path power (1 if GFSK path, 0 otherwise)
    burstDuration = N * burstMult / samplingRate;   # Burst duration [s]
    nbHops      = 1+Int(duration ÷ burstDuration);
#	@show nbHops
  
	duration 	= burstDuration*nbHops      #ensure no strange thing at end of file ******
    nbSample = Int(round(duration*samplingRate))
	#@show duration
	#@show burstDuration
    nbSymbs     = Int(round(duration * channelBW));
	d			= Array{Complex{Cfloat}}(undef,0);
    c			= Array{Complex{Cfloat}}(undef,0);
    pulseTrain	= Array{Complex{Cfloat}}(undef,nbSample);
	modSeq 		= zeros(Int64,nbHops);
    # ----------------------------------------------------
    # --- Generation of GFSK symbols
    # ----------------------------------------------------
    # --- Create the gaussian filter with BT = 0.5
    # We go from blackRate to samplingRate BT = Fs / Fb
    ovSMax = Int(samplingRate ÷ channelBW);
    h       = gaussDesign(1/ovSMax,1,ovSMax);
    # --- Create the raw symbol sequence
    bitSeq = rand([-1;1],Int(nbSymbs));
    # --- GMSK filtering
    # 101 ==> 111100001111
    bitOv  = repeat(bitSeq,inner=ovSMax);
    bitFilt = Float32.(DSP.filt(h,1,bitOv));
    # --- Ensuring that amplitude is 160kHz
    Δ       = 160000f0;
    elem    = length(bitFilt);
    # --- To data
    # FIXME, it this correct ? Doubt this ? n should increase like that ?
    # Temporal aspect is weird, but don't really know how it should be.
    @inbounds  pulseTrain   = [exp.( (0.0f0+2im) * π * Δ / samplingRate * bitFilt[1+n] * n) for n ∈ (0:elem-1)];

    # ----------------------------------------------------
    # --- Generation of FH
    # ----------------------------------------------------
    # --- Create sequence
	sequence = rand(3:N-2, nbHops);
    modPrime = vcat(fill(1,sleepMultOn), fill(0,sleepMultOff))
    ll = Int(ceil(length(sequence)/length(modPrime)))
    modSeq = repeat(modPrime,ll)[1:length(sequence)]
	sequence .*= modSeq;


	# --- Create time domain signal
    #d   = [exp.((0.0f0+2im) * π / N * ind * n) for ind ∈ sequence for n ∈ (0:burstMult * N - 1)];
	@inbounds @simd for ind ∈ sequence
        @inbounds @simd for n ∈ (0:burstMult * N - 1)
			if (ind == 0)
				push!(d, (0.0f0+0im));
			else
				push!(d, exp.((0.0f0+2im) * π / N * ind * n));
			end
		end
	end

    # ----------------------------------------------------
    # --- Generation of tones
    # ----------------------------------------------------
 	nbSymb      = Int(floor(duration * samplingRate / length(freqTones))*length(freqTones));
    rep = 3
    durTones    = (nbSymb ÷ length(freqTones)+1)/rep;
    @inbounds c = [1+exp.( (0.0f0+2im) * π *freq / samplingRate * n) for freq ∈ freqTones for n ∈ (0:durTones-1)];
    c = repeat(c,rep+1)
    #c = vcat(fill.(c, rep)...) 

    # ----------------------------------------------------
    # --- Mixing
    # ----------------------------------------------------
    dd = length(d)
    @inbounds d .*= g.* pulseTrain[1:dd] .+ k.*c[1:dd];
    g = nothing
    return d,sequence, resample(abs2.(c[1:dd]),(audioRate / samplingRate));
end

function repeat_prealloc(x, v)
    z = similar(x, sum(v))
    i = 0
    for (x,n) in zip(x,v), k = 1:n
        @inbounds z[i += 1] = x
    end
    return z
end


function txGFSKAudio(duration,channelBW,samplingRate,N,burstMult,sleepMultOn, sleepMultOff,k, audioRate)
    # ----------------------------------------------------
    # --- Parameters
    # ----------------------------------------------------
    #duration    = 0.2;                      # Duration in [s]
    #blackRate   = 1e6;                      # Data rate
    freqTones   = [700;900;1200;700];       # Tone injection frequency [Hz]
    #samplingRate= 80e6;                     # Full rate
    #N           = 80;                       # Number of channels
    #λ           = 345;	                    # FH repetition factor
    #k           = 0.5;                        # Intermodulation coefficient
    g           = 1-k;                        # Direct path power (1 if GFSK path, 0 otherwise)
    burstDuration = N * burstMult / samplingRate;   # Burst duration [s]

    nbHops      = 1+Int(duration ÷ burstDuration);
	#@show nbHops
	duration 	= burstDuration*nbHops      #ensure no strange thing at end of file ******
	#@show duration
	#@show burstDuration
    nbSymbs     = Int(round(duration * channelBW));
    c			= Array{Complex{Cfloat}}(undef,0);   
    # ----------------------------------------------------
    # --- Generation of tones
    # ----------------------------------------------------
 	nbSymb      = Int(floor(duration * samplingRate / length(freqTones))*length(freqTones));
    durTones    = nbSymb ÷ length(freqTones)+2;
    @inbounds c = [1+exp.( (0.0f0+2im) * π *freq / samplingRate * n) for freq ∈ freqTones for n ∈ (0:durTones-1)];

    return resample(abs2.(c),(audioRate / samplingRate));

end


function txBPSK(duration,channelBW,samplingRate,N,burstMult,sleepMultOn, sleepMultOff,k, audioRate)
   # ----------------------------------------------------
    # --- Parameters
    # ----------------------------------------------------
    #duration    = 0.2;                      # Duration in [s]
    #blackRate   = 1e6;                      # Data rate
    freqTones   = [700;900;1200;700];       # Tone injection frequency [Hz]
    #samplingRate= 80e6;                     # Full rate
    #N           = 80;                       # Number of channels
    #λ           = 345;	                    # FH repetition factor
    #k           = 0.5;                        # Intermodulation coefficient
    g           = 1-k;                        # Direct path power (1 if GFSK path, 0 otherwise)
    burstDuration = N * burstMult / samplingRate;   # Burst duration [s]

    nbHops      = 1+Int(duration ÷ burstDuration);
	#@show nbHops
	duration 	= burstDuration*nbHops      #ensure no strange thing at end of file ******
	#@show duration
	#@show burstDuration
    nbSymbs     = Int(round(duration * channelBW));
	d			= Array{Complex{Cfloat}}(undef,0);
    c			= Array{Complex{Cfloat}}(undef,0);
    pulseTrain	= Array{Complex{Cfloat}}(undef,0);
	modSeq 		= zeros(Int64,nbHops);
    # ----------------------------------------------------
    # --- Generation of BPSK symbols
    # ----------------------------------------------------


    # --- Create the raw symbol sequence
    bitSeq = rand([-1;1],Int(nbSymbs));

    Tb = 1/(channelBW/2)
    nb = N
    t2 = collect(Float32, Tb/nb:Tb/nb:Tb)          

    for n ∈ (bitSeq)
        if (n == 1)
            push!(pulseTrain, cos.((2.0f0+0im)*pi*channelBW.*t2 )... );
        else
            push!(pulseTrain, sin.((2.0f0+0im)*pi*channelBW.*t2 )... );
        end
    end


    # ----------------------------------------------------
    # --- Generation of FH
    # ----------------------------------------------------
    # --- Create sequence
	sequence = rand(3:N-2, nbHops);
    modPrime = vcat(fill(1,sleepMultOn), fill(0,sleepMultOff))
    ll = Int(ceil(length(sequence)/length(modPrime)))
    modSeq = repeat(modPrime,ll)[1:length(sequence)]
	sequence .*= modSeq;

	# --- Create time domain signal
    #d   = [exp.((0.0f0+2im) * π / N * ind * n) for ind ∈ sequence for n ∈ (0:burstMult * N - 1)];
	@inbounds @simd for ind ∈ sequence
		@inbounds @simd for n ∈ (0:burstMult * N - 1)
			if (ind == 0)
				push!(d, (0.0f0+0im));
			else
				push!(d, exp.((0.0f0+2im) * π / N * ind * n));
			end
		end
	end

    # ----------------------------------------------------
    # --- Generation of tones
    # ----------------------------------------------------
 	nbSymb      = Int(floor(duration * samplingRate / length(freqTones))*length(freqTones));
    durTones    = nbSymb ÷ length(freqTones)+2;
    @inbounds c   = [1+exp.( (0.0f0+2im) * π *freq / samplingRate * n) for freq ∈ freqTones for n ∈ (0:durTones-1)];
    # ----------------------------------------------------
    # --- Mixing
    # ----------------------------------------------------
    

    @inbounds d .*= (ones(length(d)).*1f0 + g.* pulseTrain[1:length(d)] .+ k.*c[1:length(d)]);

    return d,sequence, resample(abs2.(c),(audioRate / samplingRate));
end


function txRed(duration,channelBW,samplingRate,N,burstMult,sleepMultOn, sleepMultOff,k, audioRate)
    # ----------------------------------------------------
    # --- Parameters
    # ----------------------------------------------------
 # ----------------------------------------------------
    # --- Parameters
    # ----------------------------------------------------
    #duration    = 0.2;                      # Duration in [s]
    #blackRate   = 1e6;                      # Data rate
    freqTones   = [250;450;750];       # Tone injection frequency [Hz] 250 450 750Hz 700;900;1200
    #samplingRate= 80e6;                     # Full rate
    #N           = 80;                       # Number of channels
    #λ           = 345;	                    # FH repetition factor
    #k           = 0.5;                        # Intermodulation coefficient
    g           = 1-k;                        # Direct path power (1 if GFSK path, 0 otherwise)
    burstDuration = N * burstMult / samplingRate;   # Burst duration [s]
  
    nbHops      = 1+Int(duration ÷ burstDuration);
	duration 	= burstDuration*nbHops      #ensure no strange thing at end of file ******

    d			= Array{Complex{Cfloat}}(undef,0);
    c			= Array{Complex{Cfloat}}(undef,0);

    # ----------------------------------------------------
    # --- Generation of FH
    # ----------------------------------------------------
    # --- Create sequence
	sequence = rand(3:N-2, nbHops);
    modPrime = vcat(fill(1,sleepMultOn), fill(0,sleepMultOff))
    ll = Int(ceil(length(sequence)/length(modPrime)))
    modSeq = repeat(modPrime,ll)[1:length(sequence)]
	sequence .*= modSeq;
    

	# --- Create time domain signal
    d   = [exp.((0.0f0+2im) * π / N * ind * n) for ind ∈ sequence for n ∈ (0:burstMult * N - 1)];

     # ----------------------------------------------------
    # --- Generation of tones
    # ----------------------------------------------------
 	nbSymb      = Int(floor(duration * samplingRate / length(freqTones))*length(freqTones));
    rep = 3
    durTones    = (nbSymb ÷ length(freqTones)+1)/rep;
    @inbounds c = [1+exp.( (0.0f0+2im) * π *freq / samplingRate * n) for freq ∈ freqTones for n ∈ (0:durTones-1)];
    c = repeat(c,rep+1)
    # ----------------------------------------------------
    # --- Mixing
    # ----------------------------------------------------
   dd = length(d)


    d .*= 1 .+ k*c[1:dd];
											

    # ----------------------------------------------------
    # --- Saving parameters
    # ----------------------------------------------------
    # --- Save parameters
  
    return d,sequence, resample(abs2.(c[1:dd]),(audioRate / samplingRate));
end






# --- sqrtRaisedCosine.jl
# ---
# Description
#   Returns the Finite Impulse Response of a Square Root Raised Cosine (SRRC) filter.
#	The filter is defined by its span (evaluated in number of symbol N), its Roll-Off factor and its oversampling factor. The span corresponds to the number of symbol affected by filter before and after the center point.
#	Output is a Complex{Float32} array of size L= 2KN+1
#	SRRC definition is based on [1]
#	[1]	 3GPP TS 25.104 V6.8.0 (2004-12). http://www.3gpp.org/ftp/Specs/archive/25_series/25.104/25104-680.zip
# ---
# Syntax
#		h	= sqrtRaisedCosine(N,beta,ovS)
#			  # --- Input parameters
#					N	  : Symbol span (Int16)
#					beta  : Roll-off factor (Float32)
#					ovS	  : Oversampling rate (Int16)
# ---
# v 1.0 - Robin Gerzaguet.


function sqrtRaisedCosine(N,beta,ovS)
	# --- Final size of filter
	nbTaps	= 2 * N * ovS + 1;
	# --- Init output
	h			= zeros(Float32,nbTaps);
	counter		= 0;
	# --- Iterative SRRC definition
	for k = -N*ovS : 1 : N*ovS
		counter		 = counter + 1;
		if k == 0
			## First singular point at t=0
			h[counter]  = (1-beta) + 4*beta/pi;
		elseif abs(k) == ovS / (4*beta);
			## Second possible singular point
			h[counter]  = beta/sqrt(2)*( (1+2/pi)sin(pi/(4beta))+(1-2/pi)cos(pi/(4beta)));
		else
			## Classic SRRC formulation (see [1])
			h[counter]  = ( sin(pi*k/ovS*(1-beta)) + 4beta*k/ovS*cos(pi*k/ovS*(1+beta))) / (pi*k/ovS*(1- (4beta*k/ovS)^2) );
		end
	end
	# --- Normalize power of filter
	 h   = h ./ sqrt(sum((abs.(h)).^(2)));
	return h
end

"""
---
Circular convolution to avoid tails (! this is not a convolution there)
--- Syntax
  res = circularConv(a,b)
# --- Input parameters
- a : First input [Array{Number},N]
- b : Second input [Array{Number},M]
# --- Output parameters
- res : Result max(N,M).
# ---
# v 1.0 - Robin Gerzaguet.
"""
function circularConv(a,b)
	if length(a) < length(b)
		# First input is smaller than the second. We pad the first input
		a2 = zeros(eltype(a),length(b));
		a2[1:length(a)] .= a;
		# --- Circular conv
		res = ifft(fft(a2).*fft(b));
	elseif length(a) > length(b)
		# Second input is smaller than the second. We pad the second input
		b2 = zeros(eltype(b),length(a));
		b2[1:length(b)] .= b;
		# --- Circular conv
		res = ifft(fft(a).*fft(b2));
	else
		# --- Circular conv, as we have same size
		res = ifft(fft(a).*fft(b));
	end
	return res;
end


end
