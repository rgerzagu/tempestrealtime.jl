module Algos

include("structs.jl")
using .Structs
using FFTW
using DigitalComm
using DSP
using Statistics
using LinearAlgebra
using LoopVectorization
using Infiltrator
#using Memoize
using Wavelets


function initFilter(fInput,fOutput)
    ratio = fInput / fOutput;
    ratioFFT = 2;
    fftSize = ratioFFT * nextpow(2,ratio);
    inputFFT = zeros(Complex{Cfloat},fftSize);
    # --- Magnitude
    inputFFT[1:ratioFFT] .= 1;
    # --- Linear phase
    pulsation	= 2*pi*(0:fftSize-1)./fftSize;
	groupDelay	= - (fftSize-1)/2;
    fRep		= inputFFT .* exp.(1im .* groupDelay .* pulsation);
    #
    h = ifft(fRep);
    window = blackman(fftSize);
    #
    h .*= window;
    return h;
end

function readComplexBinary(file::String,nbEch=0)
	# ----------------------------------------------------
	# --- Reading file and get UInt8 elements
	# ----------------------------------------------------
	out = open(file,"r");
	if nbEch == 0
		x	= read(out);
	else
		x	= read(out,nbEch);
	end
	close(out);
	# ----------------------------------------------------
	# --- Convert data into Float64 flow
	# ----------------------------------------------------
	# --- Recasting to output
	# Half sample as complex data
	nbOut 	= Int(length(x)/4);
	y       = zeros(Float32,nbOut);
	cnt		= 1;
	for i = 1 : 1 : nbOut
		# --- 4 UInt becomes one Int
		xU       = (x[4(i-1)+1]+ (UInt32(x[4(i-1)+2]))<<8 + (UInt32(x[4(i-1)+3]))<<16+ (UInt32(x[4(i-1)+4]))<<24);
		# --- Int is reinterpreted as a Float32
		y[i] = reinterpret(Float32,xU);
	end
	# Depending on parity feed I or Q path
	# Plus Float64 conversion
	z 	= y[1:2:end]+1im*y[2:2:end];
	return z
end
function readComplexBinary2(file::String, nbChan=4)
	# ----------------------------------------------------
	# --- Reading file and get UInt8 elements
	# ----------------------------------------------------
	out = open(file,"r");
	x	= read(out);
	close(out);
	# ----------------------------------------------------
	# --- Convert data into Float64 flow
	# ----------------------------------------------------
	# --- Recasting to output
	# Half sample as complex data
	nbOut 	= Int(length(x)/4);
	y       = zeros(Float32,nbOut);
	z 		= zeros(Float32,nbChan,Int(nbOut/nbChan));
	cnt		= 1;
	for i = 1 : 1 : nbOut
		# --- 4 UInt becomes one Int
		xU       = (x[4(i-1)+1]+ (UInt32(x[4(i-1)+2]))<<8 + (UInt32(x[4(i-1)+3]))<<16+ (UInt32(x[4(i-1)+4]))<<24);
		# --- Int is reinterpreted as a Float32
		y[i] = reinterpret(Float32,xU);
	end
	# Depending on parity feed I or Q path
	# Plus Float64 conversion
	for i = 1 : 1 : nbChan
		z[i,:] 	=  y[i:nbChan:end];
	end
	return z
end

function populateBuffer!(buffer,sig,sampPerBuff)
    @inbounds @avx for n = 1 : 1 : sampPerBuff
        buffer[n] = sig[n];
    end
end

function normalizePower!(in::AbstractArray)
    in .= in ./ sqrt(mean(abs2.(in)));
end



# ----------------------------------------------------
# --- Core processing routines
# ----------------------------------------------------


function cwtMorlet!(xt::Array{Float32,2}, x::Array{Float32,2}, height,width) 
    #based on https://github.com/Ralf3/cwt/blob/master/wavelets.jl
    l::Int64=length(x)

    #Complex MORLET parameter
    Fb = 2.0 # bandwidth parameter 
    Fc = 0.8 # wavelet center frequency 
    c = 1.0 / sqrt(pi*Fb)

    # calculate the paramters for iteration
    db = Float32(floor(l/width) )    
    b0=0.0f0

    # main loop
    a=1.0f0
    l3=Int(3*l)
    l2=Int(l3/2)
    FX=zeros(l3)                   # define an array for convolution
    @inbounds for i=1:height          # loop for a
        b=b0
        @inbounds for k1=1:l3 # fill F
                FX[k1]=c * exp(-(((k1 - l3/2) / a)^2)/Fb) * cos(2*pi*Fc*k1)
        end
        @inbounds for j=1:width      # loop for b
          @inbounds for k=1:l  # Perform convolution 
              xt[i,j] += FX[Int(l2-b+k)]*x[k]
            end
            xt[i,j] /= sqrt(a)
            b += db
         end
        a += 2.0f0
    end
end

function savitzkyGolay(x::Vector, windowSize::Int, polyOrder::Int; deriv::Int=0)  
    #based on https://gist.github.com/lnacquaroli/c97fbc9a15488607e236b3472bcdf097
    isodd(windowSize) || throw("Window size must be an odd integer.")
    polyOrder < windowSize || throw("Polynomial order must me less than window size.")
    
    halfWindow = Int( ceil((windowSize-1)/2) )
    
    # Setup the S matrix of basis vectors
    S = zeros.(windowSize, polyOrder+1)
    for ct = 0:polyOrder
      S[:,ct+1] = (-halfWindow:halfWindow).^(ct)
    end
    
    ## Compute the filter coefficients for all orders
    G = S * pinv(S' * S)
    
    # Slice out the derivative order we want
    filterCoeffs = G[:, deriv+1] * factorial(deriv)
    
    # Pad the signal with the endpoints and convolve with filter
    paddedX = [x[1]*ones(halfWindow); x; x[end]*ones(halfWindow)]
    y = conv(filterCoeffs[end:-1:1], paddedX)
    
    # Return the valid midsection
    return y[2*halfWindow+1:end-2*halfWindow]
    
  end

function getAv(overBin)
	if  iseven(overBin)
		gapB   = Int(overBin÷2) - 1 ;
		gapA   = Int(overBin÷2) ;
	else
		gapB = Int(overBin÷2) ;
		gapA = Int(overBin÷2);
	end
	return (gapB,gapA)
end

function toFreq!(out,P,sig)
    mul!(out,P,sig);
end

function decision(in,sFFT,N)
    p = Int(round(mod(argmax(in) - 1, sFFT) / sFFT * N));
    (p == 0) && (p = N);
    return p;
end

function decisionAndKeep(in,sFFT,N,gapA,gapB,cache)
    # We use the sliding sum here, and returns 2 things
    # -> The position of the channel and the one in the oversamp FFT
	# --- Transform cont to take into account oversampling
    cache .= in;
    interval = gapB - gapA +1;
	# --- Sliding average with element around data of interest
 	@inbounds @simd for n ∈  1+gapA: sFFT - gapB
        # cache[n] = mean(@views in[n-gapA:n+gapB]);
        @inbounds @simd for index ∈  n - gapA : n + gapB
            cache[n] += in[index];
        end
        cache[n] = cache[n] / interval;
    end

    
    # --- Position in the oversampled grid
    maxPos = mod(argmax(cache) - 1, sFFT);
    (maxPos == 0) && (maxPos = sFFT);
    # --- Position in the channel grid
    p = Int( round(maxPos / sFFT * N));
    (p == 0) && (p = N); # error when sampling BW = Tx BW
    # --- Returning positions
    return (p,maxPos)
end
function initRotor(N,nbSamples)
    V = zeros(ComplexF32,nbSamples,N)
    for ind ∈ (1:N)
        V[:,ind] .= [exp(-2im * π / N * ind * n)  for n ∈ (0: nbSamples - 1)];
    end
    return V;
end
function derotor!(y,x,rotor)
    @inbounds @simd for n ∈ (1:length(x))
        y[n] = x[n] * rotor[n];
    end
end

function Rotor(sig, para, λ) 
    # --- PHY param
    samplingRate = Int32(para.sampleRate);
    N            = Int32(para.nbChannels);
    # --- Filter
    audioRendering = Float32(para.audioRate)

    nbSamples   = Int(N);         # Size of buffer processed
    sizeFFT     = Int(N);         # Size of FFT
    sampPerBuff = Int(N);


    # --- Init all buffers
    d           = zeros(Complex{Cfloat},nbSamples);
    dBB         = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},sizeFFT);
    sigMod      = zeros(Float32,sizeFFT);
    # --- Buffer
    buffer      = zeros(Complex{Cfloat},sizeFFT);
    buffer[1:sampPerBuff] .= sig[1:sampPerBuff];
   # Get a first vector to create what is required
    P   = plan_fft(buffer);
    # --- Double descend parameters
    # We use only one point per FFT
    sampleDescent = samplingRate / nbSamples/Int(λ);
    #secondDecim   = max(1,Int(floor(sampleDescent / audioRendering)));
    secondDecim   = sampleDescent / audioRendering;
    h2            = Float32.(real(initFilter(sampleDescent,4000)));
    # --- New decision parameters
    cacheDec      = Vector{Cfloat}(undef,sizeFFT);
    # ----------------------------------------------------
    # --- Acquisition
    # ----------------------------------------------------
    #@show size(sig)
    nbSeg           = Int(floor(length(sig) ÷ nbSamples ÷ λ));
    descentVector   = Vector{Cfloat}(undef,0);
    basebandD       = Vector{Complex{Cfloat}}(undef,0);
    allPower        = Vector{Cfloat}(undef,0);
    p̂               = Vector{Cfloat}(undef,0);
    
    cnt	  = 0;
    for ii ∈ (1:nbSeg-1)
        #print(".");
        # --- New buffer [coming from radio]
        #sigMod	.= 0.0;
        #sigMod = zeros(Cfloat,nbSamples);
        sigMod      = zeros(Complex{Cfloat},nbSamples);
        for iB = 1 : 1 : Int(λ)   
            d .= sig[cnt .+ (1:sampPerBuff)];
            # --- Populate calculation buffer
            populateBuffer!(buffer,d,sampPerBuff);
            # --- Switch to freq domain
            toFreq!(sigOut,P,buffer);
            # --- abs2
            #sigMod += abs2.(sigOut);
            sigMod += sigOut
            cnt += Int(sampPerBuff);
        end
        
        # --- Decision
        indexChannel = mod(argmax(abs2.(sigMod)) - 1, nbSamples);
        #indexChannel = mod(argmax((sigMod)) - 1, nbSamples);
        push!(p̂,indexChannel);
        descIndex = mod(indexChannel+1, N)
        (descIndex == 0) && (descIndex = N);
        push!(descentVector,abs2.(sigMod[descIndex]));       
        #push!(descentVector,sigMod[indexChannel+1]);       
    end
    # --- LPF
    #vcat(fill.(descentVector, Int(λ))...)	
    outputFilt      = conv(descentVector,h2);
    audioContent    = resample(real.(outputFilt),1/secondDecim)
   # audioContent    = real.(outputFilt[1:secondDecim:end]);
    audioContent    = normalizePower!(audioContent);
    return audioContent,p̂,descentVector,allPower
end


function Fft(sig, para, currentλ)

    samplingRate = Int32(para.sampleRate);
    N       = Int32(para.nbChannels);
    # --- Filter
    audioRendering = Float32(para.audioRate)

 
    # --- Detector parameters
    nbSamples   = 1024#1024;
    sizeFFT = 1024#1024;;         # Size of FFT
    sampPerBuff = 1024#1024;;           # Number of samples of buffer used for ZP
    # --- Init all buffers
    d           = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},sizeFFT);
    sigMod      = zeros(Float32,sizeFFT);
    # --- Buffer
    buffer      = zeros(Complex{Cfloat},sizeFFT);
    buffer[1:sampPerBuff] .= sig[1:sampPerBuff];
    # Get a first vector to create what is required
    P   = plan_fft(buffer);
    # --- Double descend parameters
    # We use only one point per FFT
    sampleDescent = samplingRate / nbSamples;
    #secondDecim   = max(1,Int(floor(sampleDescent / audioRendering)));
    secondDecim   = sampleDescent / audioRendering;
    h2            = Float32.(real(initFilter(sampleDescent,4000)));
    # --- New decision parameters
    overBin       = Int(floor(nbSamples / N));
    (gapA,gapB)   = getAv(overBin);
    cacheDec      = Vector{Cfloat}(undef,sizeFFT);
    # ----------------------------------------------------
    # --- Acquisition
    # ----------------------------------------------------
    #@show size(sig)
    nbSeg           = length(sig) ÷ nbSamples;
    descentVector   = Vector{Cfloat}(undef,0);
    descentVectorC  = Vector{Complex{Cfloat}}(undef,0);
    allPower        = Vector{Cfloat}(undef,0);
    p̂               = Vector{Cfloat}(undef,0)
    #for n ∈ (1:nbSeg)
    for n=1:Int(nbSamples):length(sig)-nbSamples
            #print(".");
            # --- New buffer [coming from radio]
            #d .= sig[ (n-1)*nbSamples .+ (1:nbSamples)];
            d .=  sig[n:n+nbSamples-1];   
            # --- Populate calculation buffer
            populateBuffer!(buffer,d,sampPerBuff);
            # --- Switch to freq domain

            toFreq!(sigOut,P,buffer);
            # --- abs2
            sigMod .= abs2.(sigOut);
            # --- Decision
            #indexChannel,posFFT = decisionAndKeep(sigMod,sizeFFT,N,gapA,gapB,cacheDec);

            posFFT = mod(argmax(sigMod), sizeFFT);
            
            (posFFT == 0) && (posFFT = sizeFFT);
            # --- Position in the channel grid
            p = Int( round((posFFT) / sizeFFT * N));
            (p == 0) && (p = N); # error when sampling BW = Tx BW


            # --- Masking and extraction
            # if posFFT == 50
                # sigMod[posFFT] = 0;
            # end
            # posFFT = 85;
            push!(p̂,p);
            # Hacking posFFT
           # push!(allPower,sum(sigMod));
            # push!(descentVector,sigMod[posFFT]);
            push!(descentVector,sigMod[posFFT]);
            push!(descentVectorC,sigOut[posFFT])
        end
    # --- LPF
    outputFilt      = conv(descentVector,h2);
    audioContent    = resample(real.(outputFilt),1/secondDecim)
   # audioContent    = real.(outputFilt[1:secondDecim:end]);
    audioContent    = normalizePower!(audioContent);

    return audioContent,p̂,descentVector,allPower
end

"""
From oversamp filter, returns the unspread spectrum 
See "Optimization of Hopping DFT for FS-FBMC Receivers", HusamAl-amaireh Zsolt Kollár, Signal Processing, 2021
"""
function ppnGather(y::Vector{T},window) where T
    # --- Get coefficients in freq domain 
    c = getCoeff(window);
    # --- FFT size 
    N = length(y) ÷ window;
    # --- Spreading area 
    A = -window+1 : window-1;
    # --- Output init 
    z = zeros( T, N);
    for n ∈ 1 : N 
        for (w,a) ∈ enumerate(A)
                z[n] +=  c[w] * y[1 + mod(window*n + a,window*N)];
        end
    end
    return z;
end

"""
Creates coefficient of filter 
"""
function getCoeff(K)
    # --- Coefficient of filter in freq domain, based here on Phydias template
    if K == 4
        p1			= 0.97195983;
        p2			= 1/sqrt(2);
        p3          = sqrt(1-p1^2);
        P			= [p1;p2;p3];
        return [P[end:-1:1];1;P]
    end
    if K == 2
        p1 = 2/sqrt(2)    
        P  = [p1];    
        return [p1[end:-1:1];1;p1]
    end
    if K == 3
        p1 = 0.911438       
        p2 = 0.411438
        P  = [p1;p2];
        return [P[end:-1:1];1;P]
    end
end


function PPN2(sigRx, para, λ, window, low)
    nbChannel    = Int32(para.nbChannels);
    samplingRate = Int32(para.sampleRate);
    audioRendering = Float32(para.audioRate)
    currentK = Int(λ)
    # --- Define Filter 

    h               = getFBMCFilter(window,nbChannel);
    # --- Define parameters 
    p̂               = Vector{Int64}(undef,0)
    descentVector   = Vector{Cfloat}(undef,0); 
    allPower        = Vector{Cfloat}(undef,0);

    sampleDescent = samplingRate / nbChannel / Int(λ);
    #secondDecim   = max(1,Int(floor(sampleDescent / audioRendering)));
    secondDecim   = sampleDescent / audioRendering;
    h2            = Float32.(real(initFilter(sampleDescent,4000)));

    # --- Number of blocks on which we do a PPN (frame of λN)
    nbS = length(sigRx) ÷ (currentK * nbChannel) 
    # --- Number of overlapped bins in each frame 
    # We have N overlaped so last point should verify αN + KN = λN 
    α = currentK - window
    for n ∈ 1 : nbS - 1
        z   = zeros(Cfloat,nbChannel); 
        subX = sigRx[ (n-1)*currentK*nbChannel .+ (1:currentK*nbChannel)];
        for a ∈ 1 : α 
            # --- KN buffer on which we do the PPN 
            subsubX = subX[ (a-1)*nbChannel .+ (1:window*nbChannel)] ;
            # --- We do the FFT 
            y = fft(subsubX);
            # --- Frequency unspreading 
            z += ppnGather(y,window);
        end
        pos = mod(argmax(abs2.(z)) , nbChannel);
        (pos == 0) && (pos = nbChannel);
        push!(p̂,pos);
        push!(descentVector,abs2.(z[pos,1]));
    end
    outputFilt      = conv(descentVector,h2);   
    audioContent    = resample(real.(outputFilt),1/secondDecim)
    audioContent    = normalizePower!(audioContent);
    return audioContent,p̂,descentVector,allPower
end


function PPN(sigRx, para, currentλ, window, low)

    samplingRate = Int32(para.sampleRate);
    nbChannel    = Int32(para.nbChannels);
    nbChannelPPN = Int(nbChannel)
    # --- Filter
    audioRendering = Float32(para.audioRate)

    # --- PPN para
    nbTap = Int(currentλ)#2
    nbTap2 = Int(nbTap/2)
    filtLen = Int(nbChannelPPN*nbTap)   

    if window == 1
        h=sinc.(-(nbTap2-1/nbChannelPPN):1/nbChannelPPN:(nbTap2-1/nbChannelPPN)).*cosine(filtLen-1, zerophase=false);   
    elseif  window == 2
        h=sinc.(-(nbTap2-1/nbChannelPPN):1/nbChannelPPN:(nbTap2-1/nbChannelPPN)).*kaiser(filtLen-1,nbTap,zerophase=true);   
    elseif  window == 3
        h=sinc.(-(nbTap2-1/nbChannelPPN):1/nbChannelPPN:(nbTap2-1/nbChannelPPN)).*gaussian(filtLen-1,0.1,zerophase=false);   
    elseif  window == 4
        h=sinc.(-(nbTap2-1/nbChannelPPN):1/nbChannelPPN:(nbTap2-1/nbChannelPPN)).*gaussian(filtLen-1,0.33,zerophase=false);   
    elseif  window == 5
        h=sinc.(-(nbTap2-1/nbChannelPPN):1/nbChannelPPN:(nbTap2-1/nbChannelPPN)).*gaussian(filtLen-1,0.5,zerophase=false);   
    elseif  window == 6
        h=sinc.(-(nbTap2-1/nbChannelPPN):1/nbChannelPPN:(nbTap2-1/nbChannelPPN)).*gaussian(filtLen-1,1.0,zerophase=false);   
    end

    filterBank=reshape([h; 0],nbChannelPPN,nbTap);    
    sampleDescent = samplingRate / nbChannelPPN;
    #secondDecim   = max(1,Int(floor(sampleDescent / audioRendering)));
    secondDecim   = sampleDescent / audioRendering;
    h2            = Float32.(real(initFilter(sampleDescent,4000)));

    v1 = zeros(Complex{Float32},nbChannelPPN,1);
    v2 = zeros(Complex{Float32},nbChannelPPN,1);   
    out = zeros(Float32,nbChannelPPN);   
    PPNOut = zeros(Complex{Float32},nbChannelPPN,nbTap); #Int(ceil(length(sigRx)/nbChannelPPN)));
    buffer = zeros(Complex{Float32},nbChannelPPN,nbTap);
   
    descentVector   = Vector{Cfloat}(undef,0); 
    allPower        = Vector{Cfloat}(undef,0);
    p̂               = Vector{Cfloat}(undef,0)

    bufferfft       = zeros(Complex{Float32},nbTap);
    sigOut          = zeros(Complex{Float32},nbTap);
    P               = plan_fft(bufferfft);


    for n=1:Int(nbChannelPPN):length(sigRx)-nbChannelPPN
        v1=(reverse(sigRx[n:n+nbChannelPPN-1]));           
        buffer[:]=[v1 buffer[:,1:nbTap-1]];      

        for k=1:nbChannelPPN        
            v2[k]=buffer[k,:]'*filterBank[k,:];         
        end             

        for ii = nbTap : -1 : 2          
            PPNOut[:,ii].=PPNOut[:,ii-1]
        end  
        PPNOut[:,1]=  fftshift(ifft(v2))        

        for ii=1:nbChannelPPN
            if low == 0
                out[ii] = abs2.(PPNOut[ii,1]) #PPNOut[ii,1]#sum(abs2.(PPNOut[ii,:]))
            elseif low == 1
                out[ii] = sum(abs2.(PPNOut[ii,:]))
            else
                populateBuffer!(bufferfft,PPNOut[ii,:],nbTap);
                toFreq!(sigOut,P,bufferfft);
                out[ii] = sum(abs2.(sigOut))
            end
        end  

        maxPos = Int(round(mod(nbChannelPPN/2 - argmax(out) + 1, nbChannelPPN)));
        #maxPos = Int(mod(nbChannelPPN/2 - argmax(out) + 1, nbChannelPPN));
        (maxPos == 0) && (maxPos = nbChannelPPN);
        # --- Position in the channel grid
        p = Int( round(maxPos / nbChannelPPN * nbChannel));
        (p == 0) && (p = nbChannel); # error when sampling BW = Tx BW
        # --- Returning positions
          
        push!(p̂,p);     
        push!(descentVector,abs2.(PPNOut[maxPos,1]));
    end
    # --- LPF
    
    outputFilt      = conv(descentVector,h2);   
    audioContent    = resample(real.(outputFilt),1/secondDecim)
   # audioContent    = real.(outputFilt[1:secondDecim:end]);
    audioContent    = normalizePower!(audioContent);

    return audioContent,p̂,descentVector,allPower

end

function wavelett(sigRX, para, currentλ, mode, algo)
#based on Analysis and implementation of a wavelet based spectrum sensing method for lowSNR scenarios, Wavelet Transform for Spectrum Sensing in Cognitive Radio Networks

    samplingRate = Int32(para.sampleRate);
    nbChannel    = Int32(para.nbChannels);

    # --- Audio filter
    audioRendering = Float32(para.audioRate)
    sampleDescent = samplingRate / nbChannel / Int(currentλ);
    #secondDecim   = max(1,Int(floor(sampleDescent / audioRendering)));
    secondDecim   = sampleDescent / audioRendering;
    h2            = Float32.(real(initFilter(sampleDescent,4000)));
    
    descentVector   = Vector{Cfloat}(undef,0); 
    allPower        = Vector{Cfloat}(undef,0);
    p̂               = Vector{Cfloat}(undef,0)

    averagingFac    = Int(currentλ)
    inWav           = zeros(Float32,nbChannel,averagingFac);
    det             = zeros(Float32,nbChannel,averagingFac); 
    xt              = zeros(Float32,nbChannel,averagingFac); 

    bufferfft      = zeros(Complex{Float32},nbChannel);
    sigOut      = zeros(Complex{Float32},nbChannel);
    P   = plan_fft(bufferfft);



    if algo == 1 
        wt = wavelet(WT.haar)
    end

    for n=1:Int(nbChannel):length(sigRX)-nbChannel-1            
        sig = sigRX[n:n+nbChannel-1]

        populateBuffer!(bufferfft,sig,nbChannel);
        toFreq!(sigOut,P,bufferfft);
        psd = abs2.(sigOut)
        #psd = abs2.((fft(sig)))
        
        if mode == 1
            inWav[:]=[psd inWav[:,1:averagingFac-1]];  
        elseif mode == 2
            psdFilt = savitzkyGolay(psd, Int(round(nbChannel*0.3+1)), 4) 
            inWav[:]=[psdFilt inWav[:,1:averagingFac-1]];  
        elseif mode == 3
            psdFilt = savitzkyGolay(psd, Int(round(5)), 4) 
            inWav[:]=[psdFilt inWav[:,1:averagingFac-1]];  
        end

        if algo == 1 
            xt = dwt(inWav, wt)
        elseif algo == 2
            cwtMorlet!(xt, inWav, nbChannel,averagingFac)
        end


        det .= xt-inWav
        p = Int(argmin(sum(det,dims = 2)[:]))
        minPos = Int(mod(p-1, nbChannel));
        (minPos == 0) && (minPos = nbChannel);

        push!(p̂,minPos);       

        descIndex = mod(minPos+1, nbChannel)
        (descIndex == 0) && (descIndex = nbChannel);
        push!(descentVector,abs2.(psd[descIndex]));   
    end

        # --- LPF    
        outputFilt      = conv(descentVector,h2);   
        audioContent    = resample(real.(outputFilt),1/secondDecim)
        # audioContent    = real.(outputFilt[1:secondDecim:end]);
        audioContent    = normalizePower!(audioContent);

    return audioContent,p̂,descentVector,allPower
end


end