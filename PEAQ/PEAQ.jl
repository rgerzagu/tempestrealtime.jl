
module PEAQ

# ----------------------------------------------------
## --- Core module loading
# ----------------------------------------------------
using FFTW
using LinearAlgebra
using Statistics
using DSP
using JLD2
using Distributed
using Infiltrator
using LoopVectorization
using SimplePlot
using Plotly
using Statistics
using PGFPlotsX
using StatsBase
#using MATLAB
using PortAudio #add https://github.com/JuliaAudio/PortAudio.jl
using WAV

import DigitalComm.addNoise!
using SimplePlot

#using ProgressMeter
#=
function loadPEAQ()
    eval_string("cd '/home/co/VirtualBox/Shared/Gitlab/tempestrealtime.jl/PEAQ/'");
    mat"""  
    addpath(genpath(pwd));
    """
end
=#
function visqol(ref, test)
   rr = pwd()
   rrPeaq = "$rr/../PEAQ"
  # a = `/home/co/VirtualBox/Shared/Gitlab/visqol/bazel-bin/visqol --reference_file /home/co/VirtualBox/Shared/Gitlab/tempestrealtime.jl/benchmark/$(ref).wav --verbose --degraded_file /home/co/VirtualBox/Shared/Gitlab/tempestrealtime.jl/benchmark/$(test).wav --similarity_to_quality_model /home/co/VirtualBox/Shared/Gitlab/visqol/model/libsvm_nu_svr_model.txt`
   a = `$rrPeaq/visqol --reference_file $(ref).wav --verbose --degraded_file $(test).wav --similarity_to_quality_model $rrPeaq/libsvm_nu_svr_model.txt`
  
   #path = "/home/co/VirtualBox/Shared/Gitlab/visqol/bazel-bin"

   strr = Vector{String}(undef,51)
   b = readchomp(a)
   ii = 1

   for line in eachline(IOBuffer(b))
      strr[ii] = line
      #println(line)
      ii = ii+1
    end

@show similarity = parse(Float32,split(strr[49],'|')[3])
@show MOS =    parse(Float32,split(strr[6],':')[2])
bande1=  parse(Float32,split(strr[15],'|')[2][1:end-3])
bande2=  parse(Float32,split(strr[17],'|')[2][1:end-3])
bande3=  parse(Float32,split(strr[20],'|')[2][1:end-3])
bandeex= parse(Float32,split(strr[41],'|')[2][1:end-3])
bandel= parse(Float32,split(strr[11],'|')[2][1:end-3]) 
eval = (bande1 + bande2 + bande3)/3 - bandeex

   #b = readchomp(a)[54:end]
   return eval#parse(Float32, b)
end

#=

function bench()
   nBit = 100
   snrRange = -90:1:90
   loadPEAQ();   
   

   Or, fs = wavread("ref3tones.wav")
   test = Array{Float64}(undef, Int(length(Or)))
   odgAll = Vector{Float64}(undef,0)
   BandwidthRef = Vector{Float64}(undef,0)
   BandwidthTest = Vector{Float64}(undef,0)
   totalNMR = Vector{Float64}(undef,0)
   WinModDiff1 = Vector{Float64}(undef,0)
   ADB = Vector{Float64}(undef,0)
   EHS = Vector{Float64}(undef,0)
   AvgModDiff1 = Vector{Float64}(undef,0)
   AvgModDiff2 = Vector{Float64}(undef,0)
   RmsNoiseLoud = Vector{Float64}(undef,0)
   MFPD = Vector{Float64}(undef,0)
   RelDistFrames = Vector{Float64}(undef,0)
  
   for iSnr = 1 : length(snrRange)
      currentSNR=snrRange[iSnr];	
      mov = zeros(Float64,12)
      for ii = 1 : nBit
         addNoise!(test, Or[:],currentSNR);	
         wavwrite(test, "test.wav", Fs=8000);
         mov1 = evalPEAQ("../benchmark/ref3tones.wav","../benchmark/test.wav")

         mov[:] += mov1[:]./nBit
      end
      
      push!(odgAll,mov[1]);
      push!(BandwidthRef,mov[2]);
      push!(BandwidthTest,mov[3]);
      push!(totalNMR,mov[4]);
      push!(WinModDiff1,mov[5]);
      push!(ADB,mov[6]);
      push!(EHS,mov[7]);
      push!(AvgModDiff1,mov[8]);
      push!(AvgModDiff2,mov[9]);
      push!(RmsNoiseLoud,mov[10]);
      push!(MFPD,mov[11]);
      push!(RelDistFrames,mov[12]);
   end
   SimplePlot.plotList([odgAll,BandwidthRef,BandwidthTest,totalNMR,WinModDiff1,ADB,EHS,AvgModDiff1,AvgModDiff2,RmsNoiseLoud,MFPD,RelDistFrames], ["odg","BandwidthRef","BandwidthTest","totalNMR","WinModDiff1","ADB","EHS","AvgModDiff1","AvgModDiff2","RmsNoiseLoud","MFPD","RelDistFrames"])
   tone3 = [odgAll,BandwidthRef,BandwidthTest,totalNMR,WinModDiff1,ADB,EHS,AvgModDiff1,AvgModDiff2,RmsNoiseLoud,MFPD,RelDistFrames]


   Or, fs = wavread("ref3tones10fois.wav")
   test = Array{Float64}(undef, Int(length(Or)))
   odgAll = Vector{Float64}(undef,0)
   BandwidthRef = Vector{Float64}(undef,0)
   BandwidthTest = Vector{Float64}(undef,0)
   totalNMR = Vector{Float64}(undef,0)
   WinModDiff1 = Vector{Float64}(undef,0)
   ADB = Vector{Float64}(undef,0)
   EHS = Vector{Float64}(undef,0)
   AvgModDiff1 = Vector{Float64}(undef,0)
   AvgModDiff2 = Vector{Float64}(undef,0)
   RmsNoiseLoud = Vector{Float64}(undef,0)
   MFPD = Vector{Float64}(undef,0)
   RelDistFrames = Vector{Float64}(undef,0)
  
   for iSnr = 1 : length(snrRange)
      currentSNR=snrRange[iSnr];	
      mov = zeros(Float64,12)
      for ii = 1 : nBit
         addNoise!(test, Or[:],currentSNR);	
         wavwrite(test, "test.wav", Fs=8000);
         mov1 = evalPEAQ("../benchmark/ref3tones10fois.wav","../benchmark/test.wav")

         mov[:] += mov1[:]./nBit
      end
      
      push!(odgAll,mov[1]);
      push!(BandwidthRef,mov[2]);
      push!(BandwidthTest,mov[3]);
      push!(totalNMR,mov[4]);
      push!(WinModDiff1,mov[5]);
      push!(ADB,mov[6]);
      push!(EHS,mov[7]);
      push!(AvgModDiff1,mov[8]);
      push!(AvgModDiff2,mov[9]);
      push!(RmsNoiseLoud,mov[10]);
      push!(MFPD,mov[11]);
      push!(RelDistFrames,mov[12]);
   end
   SimplePlot.plotList([odgAll,BandwidthRef,BandwidthTest,totalNMR,WinModDiff1,ADB,EHS,AvgModDiff1,AvgModDiff2,RmsNoiseLoud,MFPD,RelDistFrames], ["odg","BandwidthRef","BandwidthTest","totalNMR","WinModDiff1","ADB","EHS","AvgModDiff1","AvgModDiff2","RmsNoiseLoud","MFPD","RelDistFrames"])
   tone30 = [odgAll,BandwidthRef,BandwidthTest,totalNMR,WinModDiff1,ADB,EHS,AvgModDiff1,AvgModDiff2,RmsNoiseLoud,MFPD,RelDistFrames] 

   return tone3, tone30
end

function plot(tone3, tone30)

	linestyle = ["{thick},{loosely dotted}" "{thick},{densely dotted}" "{solid}" "{dashed}" "{thick},{solid}"]
	MOV = ["odgAll","BandwidthRef","BandwidthTest","totalNMR","WinModDiff1","ADB","EHS","AvgModDiff1","AvgModDiff2","RmsNoiseLoud","MFPD","RelDistFrames"]
  
   snrRange = collect(-90:1:90)[:]

	for ii = 1:1:length(MOV)

      @pgf plt = Axis({
         height      = "3in",
         width       = "4in",
         grid, 
         yminorgrids,
         xminorgrids,
         xlabel      = "SNR [dB]",
         #ylabel      = "ODG",
         #ylabel      = "NMR",
         #ylabel      = "ADB",
         ylabel      = "$(MOV[ii])",
         #title       = "Channel error rate, GFSK, 3 tone (h 0.01), 250 ps async",
         #title       = "Channel error rate, no modulation, in sync",
         #title       = "Objective Difference Grade  \$ \\in \$ [-4 0], GFSK, 3 tone N 40, \$ \\lambda \$ = K = 100, in sync",
         #title       = "Noise-to-mask ratio \$ \\in \$ [-24.045116 16.212030], GFSK, 3 tone N 40, \$ \\lambda \$ = K = 100, in sync",
         #title       = "Average block distortion \$ \\in \$ [-0.206623 2.886017], GFSK, 3 tone N 40, \$ \\lambda \$ = K = 100, in sync",
         title       = "$(MOV[ii])",							
         #ymin		= 1e-5,	
         legend_style= "{at={(1.1,-0.1)},anchor=south west,legend cell align=left,align=left,draw=white!15!black}"
         },	);


		leg = "3 tones";	
		col	  = getColorLatex(1);
		push!(plt,@pgf PGFPlotsX.Plot({color=col,mark="no mark", style=linestyle[5]},Table([snrRange,tone3[ii]])));
		push!(plt,@pgf LegendEntry(leg));
      leg = "3 tones repeated";
      col	  = getColorLatex(2);
		push!(plt,@pgf PGFPlotsX.Plot({color=col,mark="no mark", style=linestyle[5]},Table([snrRange,tone30[ii]])));
		push!(plt,@pgf LegendEntry(leg));

      display(plt);
      pgfsave("plots_$(MOV[ii]).pdf", plt)	

	end

end



function evalPEAQ(ref, test)
  # eval_string("cd '/home/co/VirtualBox/Shared/Gitlab/tempestrealtime.jl/PEAQ/'");
   #eval_string("addpath(genpath(pwd));");
  # mat"""  
  # addpath(genpath(pwd));
  # """

   #mxcall( :addpath(genpath(pwd)); )

   odgO, movbO = mxcall(:PQevalAudio_fn, 2, ref, test)


 #  mat"""  
  # addpath(genpath(pwd));
 ##  [odg, movb] = PQevalAudio_fn($ref, $test);
 #  """
 #  odgO = @mget odg
 #  movbO = @mget movb
   return [odgO movbO]
end
=#

end #end module
