clear; clc; 

cd '/home/co/VirtualBox/Shared/Gitlab/tempestrealtime.jl/PEAQ/'
addpath(genpath(pwd))

ref = 'reference.wav' % 16 times oversampling
test = 'wavelet.wav' % 4 times oversampling
[odg, movb] = PQevalAudio_fn(ref, test)

