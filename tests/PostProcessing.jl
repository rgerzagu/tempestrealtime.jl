module PostProcessing



using FFTW
using DSP
using JLD2
using Statistics
using TempestRealTime
using AbstractSDRs
using DigitalComm
using Infiltrator



function readComplexBinary(file::String,nbEch=0)
	# ----------------------------------------------------
	# --- Reading file and get UInt8 elements
	# ----------------------------------------------------
	out = open(file,"r");
	if nbEch == 0
		x	= read(out);
	else
		x	= read(out,nbEch);
	end
	close(out);
	# ----------------------------------------------------
	# --- Convert data into Float64 flow
	# ----------------------------------------------------
	# --- Recasting to output
	# Half sample as complex data
	nbOut 	= Int(length(x)/4);
	y       = zeros(Float32,nbOut);
	cnt		= 1;
	for i = 1 : 1 : nbOut
		# --- 4 UInt becomes one Int
		xU       = (x[4(i-1)+1]+ (UInt32(x[4(i-1)+2]))<<8 + (UInt32(x[4(i-1)+3]))<<16+ (UInt32(x[4(i-1)+4]))<<24);
		# --- Int is reinterpreted as a Float32
		y[i] = reinterpret(Float32,xU);
	end
	# Depending on parity feed I or Q path
	# Plus Float64 conversion
	z 	= y[1:2:end]+1im*y[2:2:end];
	return z
end
function readComplexBinary2(file::String, nbChan=4)
	# ----------------------------------------------------
	# --- Reading file and get UInt8 elements
	# ----------------------------------------------------
	out = open(file,"r");
	x	= read(out);
	close(out);
	# ----------------------------------------------------
	# --- Convert data into Float64 flow
	# ----------------------------------------------------
	# --- Recasting to output
	# Half sample as complex data
	nbOut 	= Int(length(x)/4);
	y       = zeros(Float32,nbOut);
	z 		= zeros(Float32,nbChan,Int(nbOut/nbChan));
	cnt		= 1;
	for i = 1 : 1 : nbOut
		# --- 4 UInt becomes one Int
		xU       = (x[4(i-1)+1]+ (UInt32(x[4(i-1)+2]))<<8 + (UInt32(x[4(i-1)+3]))<<16+ (UInt32(x[4(i-1)+4]))<<24);
		# --- Int is reinterpreted as a Float32
		y[i] = reinterpret(Float32,xU);
	end
	# Depending on parity feed I or Q path
	# Plus Float64 conversion
	for i = 1 : 1 : nbChan
		z[i,:] 	=  y[i:nbChan:end];
	end
	return z
end

function populateBuffer!(buffer,sig,sampPerBuff)
    @inbounds @simd for n = 1 : 1 : sampPerBuff
        buffer[n] = sig[n];
    end
end

function main()
    # --- PHY param
    samplingRate = 100e6;
    N       = 100;
    # --- Filter
    audioRendering = 8e3;
    # --- Load data
    #sig         = readComplexBinary("test_awg2.dat");
    #sig         = readComplexBinary("test_bluetooth_6_sin.dat");
    #sig         = readComplexBinary("/home/co/Documents/test_rawsweep1000uM01.dat");
    #sig         = readComplexBinary("file_synth.dat");
    sig,         = txGaussian();
    #sig         = addNoise(sig,-15);
    #sig         .= sig .* exp.(2im*pi*61.00/samplingRate*(0:length(sig)-1));
    # --- Detector parameters
    nbSamples   = 1024;
    @show sizeFFT = 1024;         # Size of FFT
    sampPerBuff = 1024;           # Number of samples of buffer used for ZP
    # --- Init all buffers
    d           = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},sizeFFT);
    sigMod      = zeros(Float32,sizeFFT);
    # --- Buffer
    buffer      = zeros(Complex{Cfloat},sizeFFT);
    buffer[1:sampPerBuff] .= sig[1:sampPerBuff];
    # Get a first vector to create what is required
    P   = plan_fft(buffer);
    # --- Double descend parameters
    # We use only one point per FFT
    sampleDescent = samplingRate / nbSamples;
    secondDecim   = Int(floor(sampleDescent / audioRendering));
    h2            = real(initFilter(sampleDescent,audioRendering));
    # --- New decision parameters
    overBin       = Int(floor(nbSamples / N));
    (gapA,gapB)   = getAv(overBin);
    cacheDec      = Vector{Cfloat}(undef,sizeFFT);
    # ----------------------------------------------------
    # --- Acquisition
    # ----------------------------------------------------
    @show size(sig)
    nbSeg           = length(sig) ÷ nbSamples;
    descentVector   = Vector{Cfloat}(undef,0);
    descentVectorC  = Vector{Complex{Cfloat}}(undef,0);
    allPower        = Vector{Cfloat}(undef,0);
    p̂               = Vector{Cfloat}(undef,0)
    for n ∈ (1:nbSeg)
            #print(".");
            # --- New buffer [coming from radio]
            d .= sig[ (n-1)*nbSamples .+ (1:nbSamples)];
            # --- Populate calculation buffer
            populateBuffer!(buffer,d,sampPerBuff);
            # --- Switch to freq domain
            toFreq!(sigOut,P,buffer);
            # --- abs2
            sigMod .= abs2.(sigOut);
            # --- Decision
            indexChannel,posFFT = decisionAndKeep(sigMod,sizeFFT,N,gapA,gapB,cacheDec);
            # --- Masking and extraction
            # if posFFT == 50
                # sigMod[posFFT] = 0;
            # end
            # posFFT = 85;
            push!(p̂,indexChannel);
            # Hacking posFFT
            push!(allPower,sum(sigMod));
            # push!(descentVector,sigMod[posFFT]);
            push!(descentVector,cacheDec[posFFT]);
            push!(descentVectorC,sigOut[posFFT])
        end
    # --- LPF
    outputFilt      = conv(descentVector,h2);
    audioContent    = real.(outputFilt[1:secondDecim:end]);
    audioContent    = normalizePower!(audioContent);
    return audioContent,p̂,descentVector,allPower,descentVectorC
end

function mainRotor()
    # --- PHY param
    samplingRate = 100e6;
    N       = 100;
    λ       = 625;
    # --- Filter
    audioRendering = 8e3;
    # --- Load data
    # sig         = readComplexBinary("test_bluetooth_6_sin.dat");
    sig         = readComplexBinary("test_bluetooth_23_dehors_arduino_sin.dat");
    # sig         = readComplexBinary("test_bluetooth_11_arduino_rfB.dat");
    # --- Detector parameters
    nbSamples   = 100;         # Size of buffer processed
    sizeFFT     = 100;         # Size of FFT
    sampPerBuff = 100;           # Numbe1.r of samples of buffer used for ZP
    # --- Init all buffers
    d         = zeros(Complex{Cfloat},nbSamples);
    dBB         = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},sizeFFT);
    sigMod      = zeros(Float32,sizeFFT);
    # --- Buffer
    buffer      = zeros(Complex{Cfloat},sizeFFT);
    buffer[1:sampPerBuff] .= sig[1:sampPerBuff];
    rotor = initRotor(N,nbSamples);
    # Get a first vector to create what is required
    P   = plan_fft(buffer);
    # --- Double descend parameters
    # We use only one point per FFT
    sampleDescent = samplingRate / nbSamples;
    secondDecim   = Int(floor(sampleDescent / audioRendering));
    h2            = real(initFilter(sampleDescent,audioRendering));
    # --- New decision parameters
    overBin       = Int(floor(nbSamples / N));
    (gapA,gapB)   = getAv(overBin);
    cacheDec      = Vector{Cfloat}(undef,sizeFFT);
    # ----------------------------------------------------
    # --- Acquisition
    # ----------------------------------------------------
    @show size(sig)
    nbSeg           = length(sig) ÷ nbSamples;
    descentVector   = Vector{Cfloat}(undef,0);
    basebandD       = Vector{Complex{Cfloat}}(undef,0);
    allPower        = Vector{Cfloat}(undef,0);
    p̂               = Vector{Cfloat}(undef,0)
    for n ∈ (1:nbSeg)
            #print(".");
            # --- New buffer [coming from radio]
            d .= sig[ (n-1)*nbSamples .+ (1:nbSamples)];
            # --- Populate calculation buffer
            populateBuffer!(buffer,d,sampPerBuff);
            # --- Switch to freq domain
            toFreq!(sigOut,P,buffer);
            # --- abs2
            sigMod .= abs2.(sigOut);
            # --- Decision
            indexChannel,posFFT = decisionAndKeep(sigMod,sizeFFT,N,gapA,gapB,cacheDec);
            # De-rotor
            derotor!(dBB,d,rotor[:,indexChannel])
            # --- Masking and extraction
            push!(p̂,indexChannel);
            push!(descentVector,sigMod[posFFT]);
            push!(allPower,sum(sigMod));
            push!(basebandD,dBB...);
        end
    # --- LPF
    outputFilt = conv(descentVector,h2);
    audioContent = real.(outputFilt[1:secondDecim:end]);
    audioContent = normalizePower!(audioContent);
    return audioContent,p̂,descentVector,allPower,basebandD
end




function mainFPGA()
    # --- PHY param
    samplingRate = 100e6;
    N       = 100;
    # --- Filter
    audioRendering = 8e3;
    # --- Load data
    # sig         = readComplexBinary("test_awg2.dat");
    # sig         = readComplexBinary("test_bluetooth_6_sin.dat");
    #sig         = readComplexBinary2("/home/co/Documents/test_mxg_cplx_all_am10.dat");
	sig         = readComplexBinary2("/home/co/rfnoc/src/rfnoc-tempest/cpp/test1/build/usrp_samples.dat");
    # sig         = readComplexBinary("fEsteban Leclercqile_synth.dat");
    #sig,          = tx();
    #sig,         = addNoise(sig,-15);
    #sig         .= sig .* exp.(2im*pi*600/samplingRate*(0:length(sig)-1));
    # --- Detector parameters
    nbSamples   = 1024;


    # Get a first vector to create what is required

    # --- Double descend parameters
    # We use only one point per FFT
    sampleDescent = samplingRate / nbSamples;
    secondDecim   = Int(floor(sampleDescent / audioRendering));
    h2            = real(initFilter(sampleDescent,audioRendering));


    # ----------------------------------------------------
    # --- Acquisition
    # ----------------------------------------------------
    @show size(sig)
    descentVector   = Vector{Cfloat}(undef,0);
    allPower        = Vector{Cfloat}(undef,0);
    p̂               = Vector{Cfloat}(undef,0)
	pos               = Vector{Cfloat}(undef,0)
	p̂_prev               = Vector{Cfloat}(undef,0)

	p̂ = sig[1,:]
	pos = sig[2,:]
	p̂_prev = sig[3,:]
	allPower = sig[4,:]

	descentVector = real(sig)

    outputFilt      = conv(descentVector,h2);
    audioContent    = real.(outputFilt[1:secondDecim:end]);
    audioContent    = normalizePower!(audioContent);


	return audioContent,p̂,descentVector,p̂_prev,allPower,pos
end



















end



# function plot800(sig)
#     # (f,psd) = getSpectrum(100e6,abs2.(sig));
#     (f,psd) = getSpectrum(100e6,sig);
#     plt = plot();
#     for iN = -40 : 1 : 40
#     # for iN = 0
#         # Center freq
#         Fc = iN * 1e6;
#         # Neigthborhood
#         fMin = findfirst(f .≥ Fc);
#         fMax = findfirst(f .≥ Fc .+ -1000);
#         xAx  = range(0,1000,length=fMax-fMin);
#         plot!(plt,xAx,psd[fMin:fMax]);
#     end
#     return plt;
# end
