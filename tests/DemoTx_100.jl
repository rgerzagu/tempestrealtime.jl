module DemoTx 


using FFTW 
using DSP
using JLD2 
using Statistics
using TempestRealTime
using AbstractSDRs
include("ConvertAWG.jl")
include("Audio.jl")

struct SynthSignals
    duration::Float64;
    blackRate::Float64;
    freqTones::Array{Float64};
    samplingRate::Float64;
    N:: Int;
    λ::Int;
    burstDuration::Float64;
    nbHops::Int;
    nbSymb::Int;
    sequence::Array{Int};

end

function txAudio()
    freqTones   = [700;900;1200;700];
    samplingRate = 42e3;
    durTones= 0.5*samplingRate ÷ length(freqTones);
    c   = [1+exp.(2im * π *freq / samplingRate * n) for freq ∈ freqTones for n ∈ (0:durTones-1)];
    return abs2.(c)
end

function tx()
    # ----------------------------------------------------
    # --- Parameters
    # ---------------------------------------------------- 
    duration    = 0.5;
    blackRate   = 1e6;
    freqTones   = [700;900;1200;700];
    samplingRate= 80e6;
    N           = 80;
    λ           = 60;
    burstDuration = N * λ / samplingRate;
    nbHops     = 1+Int(duration ÷ burstDuration);
    # ----------------------------------------------------
    # --- Generation of FH
    # ---------------------------------------------------- 
    nbSymb      = Int(floor(duration * samplingRate / length(freqTones))*length(freqTones));
    # --- Create sequence
	sequence = rand(1:N, nbHops);
	# --- Create time domain signal 
    d   = [exp.(2im * π / N * ind * n) for ind ∈ sequence for n ∈ (0:λ * N - 1)];
    # ----------------------------------------------------
    # --- Generation of tones 
    # ---------------------------------------------------- 
    durTones    = Int(ceil(length(d) / length(freqTones)));
    c   = [1+exp.(2im * π *freq / samplingRate * n) for freq ∈ freqTones for n ∈ (0:durTones-1)];
    # ----------------------------------------------------
    # --- Mixing
    # ---------------------------------------------------- 
    @show size(d), size(c)
    d .*= c[1:length(d)];
    # ----------------------------------------------------
    # --- Saving parameters 
    # ---------------------------------------------------- 
    # --- Save parameters 
    synth = SynthSignals(
        duration,
        blackRate,
        freqTones,
        samplingRate,
        N,
        λ,
        burstDuration,
        nbHops,
        nbSymb,
        sequence,
    );
    # --- create AWG signal 
     convertAWG("file_synth_100.dat",d);
     @save "struc_synth_100.jld2" synth;
    return d,synth,c;
end

function testRx2()
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    sig,synth = tx();
    # --- PHY param 
    nbHop   = synth.nbHops;
    N       = synth.N;
    λ       = synth.λ
    nbEst   = nbHop * λ;
    p̂       = zeros(Int,nbEst);
    # --- Filter 
    audioFreq   = 42e3;
    h           = initFilter(synth.samplingRate,audioFreq);
    decim       = Int(synth.samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = 1024;
    d           = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Cfloat},nbSamples);
    # --- Buffer 
    buffer      = zeros(Complex{Cfloat},nbSamples);
    buffer[1:N] .= sig[1:N];
    # Get a first vector to create what is required 
    P   = plan_fft(buffer);
    V   = initRotor(N,nbSamples);
    out = zeros(Complex{Cfloat}, nbSamples * λ * nbHop)
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    @show size(sig)
    nbSeg = length(sig) ÷ nbSamples;
    @show nbEst,nbSeg
    for n ∈ (1:nbSeg)
        #print(".");
        # --- New buffer with ZP
        d .= sig[ (n-1)*nbSamples .+ (1:nbSamples)]
        #d[1:N] .= sig[ (n-1)*N .+ (1:N)]
        #buffer[1:N]  .= d
        # --- Switch to freq domain 
        toFreq!(sigOut,P,d);
        # --- abs2 
        sigMod .= abs2.(sigOut);
        # --- Decision 
        p̂[n] = decision(sigMod,nbSamples,N);
        # --- Rotor 
        derotor!(sigBB,d,V[:,p̂[n]]);
        out[(n-1)*nbSamples .+ (1:nbSamples)] = sigBB;
    end
    # --- LPF 
    outputFilt = conv(out,h);
    audioContent = abs2.(outputFilt[1:decim:end]);
    @show sum( xor.(p̂[1:λ:end],synth.sequence));
    return audioContent,p̂
end


function testRx3()
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    sig,synth = tx();
    # --- PHY param 
    nbHop   = synth.nbHops;
    N       = synth.N;
    λ       = synth.λ
    nbEst   = nbHop * λ;
    p̂       = zeros(Int,nbEst);
    # --- Filter 
    audioFreq   = 42e3;
    audioRendering = 8e3;
    h           = initFilter(synth.samplingRate,audioFreq);
    decim       = Int(synth.samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = synth.N;
    fftSize     = synth.N;
    d           = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Cfloat},nbSamples);
    # --- Buffer 
    buffer      = zeros(Complex{Cfloat},nbSamples);
    buffer[1:N] .= sig[1:N];
    # Get a first vector to create what is required 
    P   = plan_fft(buffer);
    V   = initRotor(N,nbSamples);
    out = zeros(Complex{Cfloat}, nbSamples * λ * nbHop)
    # --- Double descend parameters 
    # We use only one point per FFT 
    sampleDescent = synth.samplingRate / fftSize;
    secondDecim = Int(floor(sampleDescent / audioRendering));
    h2  = initFilter(sampleDescent,audioRendering)
    # --- New decision parameters 
    overBin = Int(floor(fftSize / synth.N));
    (gapA,gapB) = getAv(overBin);
    cacheDec  = Vector{Complex{Cfloat}}(undef,fftSize);
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    @show size(sig)
    nbSeg = length(sig) ÷ nbSamples;
    @show nbEst,nbSeg
    nS = 0;
    gB = 0;
    descentVector = Vector{Complex{Cfloat}}(undef,0);
    for n ∈ (1:nbSeg)
            #print(".");
            # --- New buffer with ZP
            d .= sig[ (n-1)*nbSamples .+ (1:nbSamples)]
            # --- Switch to freq domain 
            toFreq!(sigOut,P,d);
            # --- abs2 
            sigMod .= abs2.(sigOut);
            # --- Decision 
            indexChannel,posFFT = decisionAndKeep(sigMod,fftSize,N,gapA,gapB,cacheDec);
            p̂[n]  = indexChannel;
            # --- Masking and extraction 
            push!(descentVector,sigOut[posFFT]);
            # --- Rotor 
            # derotor!(sigBB,d,V[:,indexChannel]);
            # out[gB+(m-1)*nbSamples .+ (1:nbSamples)] = sigBB;
        end
    # --- LPF 
    outputFilt = conv(descentVector,h2);
    audioContent = real.(outputFilt[1:secondDecim:end]);
    audioContent = normalizePower!(audioContent);
    return audioContent,p̂,descentVector
end


function testRx3_1024()
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    sig,synth = tx();
    # --- PHY param 
    nbHop   = synth.nbHops;
    N       = synth.N;
    λ       = synth.λ
    nbEst   = nbHop * λ;
    p̂       = zeros(Int,nbEst);
    # --- Filter 
    audioFreq   = 42e3;
    audioRendering = 8e3;
    h           = initFilter(synth.samplingRate,audioFreq);
    decim       = Int(synth.samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = synth.N;
    fftSize     = synth.N;
    d           = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Cfloat},nbSamples);
    # --- Buffer 
    buffer      = zeros(Complex{Cfloat},nbSamples);
    buffer[1:N] .= sig[1:N];
    # Get a first vector to create what is required 
    P   = plan_fft(buffer);
    V   = initRotor(N,nbSamples);
    out = zeros(Complex{Cfloat}, nbSamples * λ * nbHop)
    # --- Double descend parameters 
    # We use only one point per FFT 
    sampleDescent = synth.samplingRate / fftSize;
    secondDecim = Int(floor(sampleDescent / audioRendering));
    h2  = initFilter(sampleDescent,audioRendering)
    # --- New decision parameters 
    overBin = Int(floor(fftSize / synth.N));
    (gapA,gapB) = getAv(overBin);
    cacheDec  = Vector{Complex{Cfloat}}(undef,fftSize);
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    @show size(sig)
    nbSeg = length(sig) ÷ nbSamples;
    @show nbEst,nbSeg
    nS = 0;
    gB = 0;
    descentVector = Vector{Complex{Cfloat}}(undef,0);
    for n ∈ (1:nbSeg)
            #print(".");
            # --- New buffer with ZP
            d .= sig[ (n-1)*nbSamples .+ (1:nbSamples)]
            # --- Switch to freq domain 
            toFreq!(sigOut,P,d);
            # --- abs2 
            sigMod .= abs2.(sigOut);
            # --- Decision 
            indexChannel,posFFT = decisionAndKeep(sigMod,fftSize,N,gapA,gapB,cacheDec);
            p̂[n]  = indexChannel;
            # --- Masking and extraction 
            push!(descentVector,sigOut[posFFT]);
            # --- Rotor 
            # derotor!(sigBB,d,V[:,indexChannel]);
            # out[gB+(m-1)*nbSamples .+ (1:nbSamples)] = sigBB;
        end
    # --- LPF 
    outputFilt = conv(descentVector,h2);
    audioContent = real.(outputFilt[1:secondDecim:end]);
    audioContent = normalizePower!(audioContent);
    return audioContent,p̂,descentVector
end
function testRx()
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    sig,synth = tx();
    # --- PHY param 
    nbHop   = synth.nbHops;
    N       = synth.N;
    λ       = synth.λ
    nbEst   = nbHop * λ;
    p̂       = zeros(Int,nbEst);
    # --- Filter 
    audioFreq   = 42e3;
    h           = initFilter(synth.samplingRate,audioFreq);
    decim       = Int(synth.samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = 1024;
    d           = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Cfloat},N);
    # --- Buffer 
    buffer      = zeros(Complex{Cfloat},nbSamples);
    buffer[1:N] .= sig[1:N];
    # Get a first vector to create what is required 
    P   = plan_fft(buffer);
    V   = initRotor(N,N);
    out = zeros(Complex{Cfloat}, nbSamples * λ * nbHop)
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    @show nbEst
    @show size(sig)
    for n ∈ (1:nbEst)
        #print(".");
        # --- New buffer with ZP
        # d .= sig[ (n-1)*nbSamples .+ (1:nbSamples)]
        d[1:N] .= sig[ (n-1)*N .+ (1:N)]
        #buffer[1:N]  .= d
        # --- Switch to freq domain 
        toFreq!(sigOut,P,d);
        # --- abs2 
        sigMod .= abs2.(sigOut);
        # --- Decision 
        p̂[n] = decision(sigMod,nbSamples,N);
        # --- Rotor 
        derotor!(sigBB,d[1:N],V[:,p̂[n]]);
        out[(n-1)*N .+ (1:N)] = sigBB;
    end
    # --- LPF 
    outputFilt = conv(out,h);
    audioContent = abs2.(outputFilt[1:decim:end]);
    @show sum( xor.(p̂[1:λ:end],synth.sequence));
    return audioContent,p̂
end


function rx2()
    @load "struc_synth_100.jld2" synth;
    # ----------------------------------------------------
    # --- Radio
    # ---------------------------------------------------- 
	carrierFreq		= 700e6;
	gain			= 6; 
    samplingRate    = 100e6;
    sdr             = "uhd";
	global radio			= openSDR(sdr,carrierFreq, samplingRate, gain); 
    radioSize       = radio.rx.packetSize;
    acqui           = 2;      # Desired time of acquisition in second 
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    # --- PHY param 
    N       = 100;synth.N;
    # λ       = synth.λ
    # --- Filter 
    audioFreq   = 42e3;
    h           = initFilter(samplingRate,8e3);
    decim       = Int(samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = 1024; # Buffer size process by radio in one iteration
    fftSize     = 1024; # FFT size applied
    packetRec   = 32768;
    bigBuffer   = zeros(Complex{Cfloat},packetRec);
    sig         = zeros(Complex{Cfloat},packetRec);
    d           = zeros(Complex{Cfloat},nbSamples);
    buffer      = zeros(Complex{Cfloat},fftSize);
    sigOut      = zeros(Complex{Cfloat},fftSize);
    sigMod      = zeros(Float32,fftSize);
    sigBB       = zeros(Complex{Cfloat},nbSamples);
    # --- Testing radio 
    recv!(sig,radio);
    buffer[1:nbSamples] .= sig[1:nbSamples];
    # Get a first vector to create what is required 
    P                   = plan_fft(buffer);
    V                   = initRotor(N,nbSamples);
    bufferPerBigBuffer  = Int( packetRec ÷ nbSamples)
    @show nbSeg = Int(acqui * samplingRate ÷ packetRec);
    # usefull vector init
    out = zeros(Complex{Cfloat}, Int(acqui*samplingRate))
    @show nbSeg * bufferPerBigBuffer
    p̂       = zeros(Int,nbSeg*bufferPerBigBuffer);
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    nS = 0;
    gB = 0;
    print(radio)
    for n ∈ (1:nbSeg)
        # We get a big buffer from the radio and compute each subvector
        p  =recv!(sig,radio);
        nS += p;
        for m ∈ (1:bufferPerBigBuffer)
            # --- New buffer with ZP
            d .= @views sig[ (m-1)*nbSamples .+ (1:nbSamples)];
            buffer[1:nbSamples] .= d;
            # --- Switch to freq domain 
            toFreq!(sigOut,P,buffer);
            # --- abs2 
            sigMod .= abs2.(sigOut);
            # --- Decision 
            indexChannel = decision(sigMod,fftSize,N);
            p̂[(n-1)*bufferPerBigBuffer + m]  = indexChannel;
            # --- Rotor 
            derotor!(sigBB,d,V[:,indexChannel]);
            out[gB+(m-1)*nbSamples .+ (1:nbSamples)] = sigBB;
        end
        gB += packetRec;
        if nS > Int(acqui * samplingRate) 
            @show n,nbSeg
            break;
        end
    end
    # --- LPF 
    outputFilt = conv(out,h);
    audioContent = abs2.(outputFilt[1:decim:end]);
    audioContent = normalizePower!(audioContent);
    close(radio)
    return audioContent,p̂,out
end

function rxContinuousPlay()
    @load "struc_synth.jld2" synth;
    # ----------------------------------------------------
    # --- Radio
    # ---------------------------------------------------- 
	carrierFreq		= 2000e6;
	gain			= 20; 
    samplingRate    = 20e6;
    sdr             = "uhd";
	radio			= openSDR(sdr,carrierFreq, samplingRate, gain); 
    radioSize       = radio.rx.packetSize;
    acqui           = 0.1;      # Desired time of acquisition in second 
    print(radio)
    # ----------------------------------------------------
    # --- Audio 
    # ----------------------------------------------------    
    stream = Audio.createAudioBuffer();
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    # --- PHY param 
    N       = 100;
    # λ       = synth.λ
    # --- Filter 
    audioFreq   = 42e3;
    h           = initFilter(samplingRate,42e3);
    decim       = Int(samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = 1024;
    packetRec   = 32768;
    bigBuffer   = zeros(Complex{Cfloat},packetRec);
    sig         = zeros(Complex{Cfloat},packetRec);
    d           = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Cfloat},nbSamples);
    # --- Testing radio 
    recv!(sig,radio);
    # Get a first vector to create what is required 
    P                   = plan_fft(sig[1:nbSamples]);
    V                   = initRotor(N,nbSamples);
    bufferPerBigBuffer  = Int( packetRec ÷ nbSamples)
    @show nbSeg = Int(acqui * samplingRate ÷ packetRec);
    # usefull vector init
    out             = zeros(Complex{Cfloat}, nbSamples * bufferPerBigBuffer)
    cache           = zeros(Complex{Cfloat}, nbSamples * bufferPerBigBuffer+ length(h)-1)
    nbAudio         = length(cache) ÷ decim;
    audioContent    = zeros(Cfloat,nbAudio)
    @show nbSeg * bufferPerBigBuffer
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    nS = 0;
    gB = 0;
    audioDur = 1;
    audioSamp = audioDur * audioFreq;
    bufferAudio = Vector{Cfloat}(undef,0);

   
    try
        while true
            # We get a big buffer from the radio and compute each subvector
            p  =recv!(sig,radio);
            nS += p;
            for m ∈ (1:bufferPerBigBuffer)
                # --- New buffer with ZP
                d .= sig[ (m-1)*nbSamples .+ (1:nbSamples)]
                # --- Switch to freq domain 
                toFreq!(sigOut,P,d);
                # --- abs2 
                sigMod .= abs2.(sigOut);
                # --- Decision 
                indexChannel = decision(sigMod,nbSamples,N);
                # --- Rotor 
                derotor!(sigBB,d,V[:,indexChannel]);
                out[(m-1)*nbSamples .+ (1:nbSamples)] = sigBB;
            end
            # @async begin 
                audioContent = audioProcessing!(audioContent,out,h,cache,decim)
                push!(bufferAudio,audioContent...)
                if length(bufferAudio) > audioSamp
                    # Audio.play(repeat(createAudioBuffer,4))
                    # @async Audio.play(stream,bufferAudio)
                    bufferAudio = Vector{Cfloat}(undef,0);
                end
                # @show size(audioContent)
                # Audio.play(stream,audioContent)
            # end
            # if nS > Int(acqui * samplingRate) 
                # break;
            # end
        end
    catch exception;
        @info "Interruption after $nS processed samples";
    # --- LPF 
    close(radio);
    close(stream);
        rethrow(exception)
    end
    close(radio);
    close(stream);
end

function audioProcessing!(audioContent,out,h,cache,decim)
    cache      = conv(out,h);
    audioContent    = abs2.(cache[1:decim:end]);
    normalizePower!(audioContent);
    return audioContent
end


function rx()
    @load "struc_synth.jld2" synth;
    # ----------------------------------------------------
    # --- Radio
    # ---------------------------------------------------- 
	carrierFreq		= 770e6;
	gain			= 25.0; 
    samplingRate    =  4e6;
    sdr             = "e310";
	global radio			= openSDR(sdr,carrierFreq, samplingRate, gain); 
	radioSize       = radio.rx.packetSize;
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    # --- PHY param 
    nbHop   = synth.nbHops;
    N       = synth.N;
    λ       = synth.λ
    nbEst   = nbHop * λ;
    p̂       = zeros(Int,nbEst);
    # --- Filter 
    audioFreq   = 42e3;
    h           = initFilter(synth.samplingRate,audioFreq);
    decim       = Int(synth.samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = 1024;
    d           = zeros(Complex{Cfloat},N);
    sigOut      = zeros(Complex{Cfloat},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Cfloat},nbSamples);
    # --- Testing radio 
    recv!(d,radio);
    # --- Buffer 
    buffer      = zeros(Complex{Cfloat},nbSamples);
    buffer[1:N] .= d[1:N];
    # Get a first vector to create what is required 
    P   = plan_fft(buffer);
    V   = initRotor(N,N);
    out = zeros(Complex{Cfloat}, nbSamples * λ * nbHop)
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    @show nbEst
    for n ∈ (1:15600)
        #print(".");
        # --- New buffer with ZP
        recv!(d,radio)
        buffer[1:N]  .= d
        # --- Switch to freq domain 
        toFreq!(sigOut,P,buffer);
        # --- abs2 
        sigMod .= abs2.(sigOut);
        # --- Decision 
        p̂[n] = decision(sigMod,nbSamples,N);
        # --- Rotor 
        derotor!(sigBB,d,V[:,p̂[n]]);
        out[(n-1)*nbSamples .+ (1:nbSamples)] = sigBB;
    end
    # --- LPF 
    outputFilt = conv(out,h);
    audioContent = abs2.(outputFilt[1:decim:end]);
    @show sum( xor.(p̂[1:λ:end],synth.sequence));
    close(radio);
    return audioContent,p̂
end






function rx3()
    @load "struc_synth_100.jld2" synth;
    # ----------------------------------------------------
    # --- Radio
    # ---------------------------------------------------- 
	carrierFreq		= 700e6;
	gain			= 6; 
    samplingRate    = 100e6;
    sdr             = "uhd";
	global radio			= openSDR(sdr,carrierFreq, samplingRate, gain); 
    radioSize       = radio.rx.packetSize;
    acqui           = 2;      # Desired time of acquisition in second 
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    # --- PHY param 
    N       = 100;synth.N;
    # λ       = synth.λ
    # --- Filter 
    audioFreq   = 42e3;         # For playing sounds
    audioRendering = 8e3;     # We only use 8kHz for audio rendering
    h           = initFilter(samplingRate,audioRendering);
    decim       = Int(samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = 1024; # Buffer size process by radio in one iteration
    fftSize     = 1024; # FFT size applied
    packetRec   = 32768;
    sig         = zeros(Complex{Cfloat},packetRec);
    d           = zeros(Complex{Cfloat},nbSamples);
    buffer      = zeros(Complex{Cfloat},fftSize);
    sigOut      = zeros(Complex{Cfloat},fftSize);
    sigMod      = zeros(Float32,fftSize);
    sigBB       = zeros(Complex{Cfloat},nbSamples);
    # --- Testing radio 
    recv!(sig,radio);
    buffer[1:nbSamples] .= sig[1:nbSamples];
    # Get a first vector to create what is required 
    P                   = plan_fft(buffer);
    V                   = initRotor(N,nbSamples);
    bufferPerBigBuffer  = Int( packetRec ÷ nbSamples)
    @show nbSeg = Int(acqui * samplingRate ÷ packetRec);
    # usefull vector init
    out = zeros(Complex{Cfloat}, Int(acqui*samplingRate))
    @show nbSeg * bufferPerBigBuffer
    p̂       = zeros(Int,nbSeg*bufferPerBigBuffer);
    # --- Double descend parameters 
    # We use only one point per FFT 
    sampleDescent = samplingRate / fftSize;
    secondDecim = floor(Int(sampleDescent / audioRendering));
    h2  = initFilter(sampleDescent,audioRendering)
    # --- New decision parameters 
    overBin = Int(floor(fftSize / N));
    (gapA,gapB) = getAv(overBin);
    cacheDec  = Vector{Complex{Cfloat},fftSize};
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    nS = 0;
    gB = 0;
    print(radio)
    descentVector = Vector{Complex{Cfloat}}(undef,0);
    for n ∈ (1:nbSeg)
        # We get a big buffer from the radio and compute each subvector
        p  =recv!(sig,radio);
        nS += p;
        for m ∈ (1:bufferPerBigBuffer)
            # --- New buffer with ZP
            d .= @views sig[ (m-1)*nbSamples .+ (1:nbSamples)];
            buffer[1:nbSamples] .= d;
            # --- Switch to freq domain 
            toFreq!(sigOut,P,buffer);
            # --- abs2 
            sigMod .= abs2.(sigOut);
            # --- Decision 
            indexChannel,posFFT = decisionAndKeep(sigMod,fftSize,N,gapA,gapB,cacheDec);
            p̂[(n-1)*bufferPerBigBuffer + m]  = indexChannel;
            # --- Masking and extraction 
            push!(descentVector,sigOut[posFFT]);
            # --- Rotor 
            # derotor!(sigBB,d,V[:,indexChannel]);
            # out[gB+(m-1)*nbSamples .+ (1:nbSamples)] = sigBB;
        end
        gB += packetRec;
        if nS > Int(acqui * samplingRate) 
            break;
        end
    end
    # --- LPF 
    outputFilt = conv(descentVector,h2);
    audioContent = abs2.(outputFilt[1:secondDecim:end]);
    audioContent = normalizePower!(audioContent);
    close(radio)
    return audioContent,p̂,out
end

end