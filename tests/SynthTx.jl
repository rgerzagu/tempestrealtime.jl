module SynthTx


using FFTW
using DSP
using JLD2
using Infiltrator
#using TempestRealTime
include("../src/ConvertAWG.jl")
include("Audio.jl")

struct SynthSignals
    duration::Float64;
    blackRate::Float64;
    freqTones::Array{Float64};
    samplingRate::Float64;
    N:: Int;
    λ::Int;
    burstDuration::Float64;
    nbHops::Int;
    nbSymb::Int;
    sequence::Array{Int};

end


function gaussDesign(BT,L,ovS)
    # --- Setting α from BT
    α   =  sqrt(log(2)/2)/(BT);
    # --- Time span
    t   = -L*ovS:1:L*ovS;
    # --- Filter
    h   = sqrt(pi)/α  *exp.(-(pi/ α.* t).^2);
    return h;
end

function txGaussian()
    # ----------------------------------------------------
    # --- Parameters
    # ----------------------------------------------------
    duration    = 0.2;                      # Duration in [s]
    blackRate   = 1e6;                      # Data rate
    freqTones   = [700;900;1200;700];       # Tone injection frequency [Hz]
    samplingRate= 80e6;                     # Full rate
    N           = 80;                       # Number of channels
    λ           = 345;	                    # FH repetition factor
    k           = 0.5;                        # Intermodulation coefficient
    g           = 1-k;                        # Direct path power (1 if GFSK path, 0 otherwise)
    burstDuration = N * λ / samplingRate;   # Burst duration [s]

    nbHops      = 1+Int(duration ÷ burstDuration);
	@show nbHops
	duration 	= burstDuration*nbHops      #ensure no strange thing at end of file ******
	@show duration
	@show burstDuration
    nbSymbs     = Int(round(duration * blackRate));
	d			= Array{Complex{Float64}}(undef,0);
	modSeq 		= zeros(Int64,nbHops);
    # ----------------------------------------------------
    # --- Generation of GFSK symbols
    # ----------------------------------------------------
    # --- Create the gaussian filter with BT = 0.5
    # We go from blackRate to samplingRate BT = Fs / Fb
    ovSMax = Int(samplingRate ÷ blackRate);
    h       = gaussDesign(1/ovSMax,1,ovSMax);
    # --- Create the raw symbol sequence
    bitSeq = rand([-1;1],Int(nbSymbs));
    # --- GMSK filtering
    # 101 ==> 111100001111
    bitOv  = repeat(bitSeq,inner=ovSMax);
    bitFilt = DSP.filt(h,1,bitOv);
    # --- Ensuring that amplitude is 160kHz
    Δ       = 160e3;
    elem    = length(bitFilt);
    # --- To data
    # FIXME, it this correct ? Doubt this ? n should increase like that ?
    # Temporal aspect is weird, but don't really know how it should be.
    pulseTrain   = [exp.(2im * π * Δ / samplingRate * bitFilt[1+n] * n) for n ∈ (0:elem-1)];
    # ----------------------------------------------------
    # --- Generation of FH
    # ----------------------------------------------------
    # --- Create sequence
	sequence = rand(1:N, nbHops);
	#modSeq[1:76:end].=1
	modSeq[1]=10
	modSeq[2]=0
	modSeq[3]=60
	modSeq[4]=0
	modSeq[5]=60
	modSeq[6]=0
	@show length(modSeq)

	#modSeq[3:76:end-2].=1
	#modSeq[5:76:end-4].=1


	sequence .*= modSeq;



	# --- Create time domain signal
    d   = [exp.(2im * π / N * ind * n) for ind ∈ sequence for n ∈ (0:λ * N - 1)];
	#for ind ∈ modSeq#sequence
	#	for n ∈ (0:λ * N - 1)
	#		if (ind == 0)
	#			push!(d, 0.0 + 0.0im);
	#		else
	#			push!(d, exp.(2im * π / N * ind * n));
	#		end
	#	end
	#end

    # ----------------------------------------------------
    # --- Generation of tones
    # ----------------------------------------------------
	nbSymb      = Int(floor(duration * samplingRate / length(freqTones))*length(freqTones));
    durTones    = nbSymb ÷ length(freqTones)+2;
    c   = [1+exp.(2im * π *freq / samplingRate * n) for freq ∈ freqTones for n ∈ (0:durTones-1)];
    # ----------------------------------------------------
    # --- Mixing
    # ----------------------------------------------------
	#@infiltrate
    d .*= (g.* pulseTrain .+ k.*c[1:length(pulseTrain)]);
    # # ----------------------------------------------------
    # # --- Saving parameters
    # # ----------------------------------------------------
    # --- Save parameters
    synth = SynthSignals(
        duration,
        blackRate,
        freqTones,
        samplingRate,
        N,
        λ,
        burstDuration,
        nbHops,
        nbSymb,
        sequence,
    );
    # --- create AWG signal
	ttt = string(k);
    #convertAWG("GFSK_burst_test.dat",d);
	convertAWG("/media/co/2 GO/test3.dat",d);

    @save "struc_synth_burst.jld2" synth;
    return d,synth,sequence;
    # return d,pulseTrain
end

function tx()
    # ----------------------------------------------------
    # --- Parameters
    # ----------------------------------------------------
    duration    = 0.4;
    blackRate   = 1e6;                  # Probably useless
    freqTones   = [700;900;1200;700];
    samplingRate= 80e6;
    N           = 80;
    λ           = 10;
    burstDuration = N * λ / samplingRate;
    nbHops     = 1+Int(duration ÷ burstDuration);
    # ----------------------------------------------------
    # --- Generation of FH
    # ----------------------------------------------------
    nbSymb      = Int(floor(duration * samplingRate / length(freqTones))*length(freqTones));
    # --- Create sequence
	sequence = rand(1:N, nbHops);
	# --- Create time domain signal
    d   = [exp.(2im * π / N * ind * n) for ind ∈ sequence for n ∈ (0:λ * N - 1)];
    # ----------------------------------------------------
    # --- Generation of tones
    # ----------------------------------------------------
    durTones    = nbSymb ÷ length(freqTones);
    c   = [1+exp.(2im * π *freq / samplingRate * n) for freq ∈ freqTones for n ∈ (0:durTones-1)];
    # ----------------------------------------------------
    # --- Mixing
    # ----------------------------------------------------
    d .*= c;
    # ----------------------------------------------------
    # --- Saving parameters
    # ----------------------------------------------------
    # --- Save parameters
    synth = SynthSignals(
        duration,
        blackRate,
        freqTones,
        samplingRate,
        N,
        λ,
        burstDuration,
        nbHops,
        nbSymb,
        sequence,
    );
    # --- create AWG signal
    convertAWG("file_synth.dat",d);
    @save "struc_synth.jld2" synth;
    return d,synth;
end

function rx()
    # @load "struc_synth" synth;
    d,synth  = tx();
    # --- PHY param
    nbHop   = synth.nbHops;
    N       = synth.N;
    λ       = synth.λ
    nbEst   = nbHop * λ;
    p̂   = zeros(Int,nbEst);
    # --- Filter
    audioFreq = 42e3;
    h   = initFilter(synth.samplingRate,audioFreq);
    decim = Int(synth.samplingRate ÷ audioFreq);
    # --- Detector parameters
    nbSamples   = 1024;
    sigOut      = zeros(Complex{Float32},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Float32},N);
    # --- Buffer
    buffer      = zeros(ComplexF32,nbSamples);
    buffer[1:N] .= d[1:N];
    # Get a first vector to create what is required
    P   = plan_fft(buffer);
    V   = initRotor(N,nbSamples);
    out = similar(d);
    for n ∈ (1:nbEst)
        # --- New buffer with ZP
        buffer[1:N]  .= d[(n-1)*N .+ (1:N)];
        # --- Switch to freq domain
        toFreq!(sigOut,P,buffer);
        # --- abs2
        sigMod .= abs2.(sigOut);
        # --- Decision
        p̂[n] = decision(sigMod,nbSamples,N);
        # --- Rotor
        derotor!(sigBB,buffer[1:N],V[:,p̂[n]]);
        out[(n-1)*N .+ (1:N)] = sigBB;
    end
    # --- LPF
    outputFilt = conv(out,h);
    audioContent = abs2.(outputFilt[1:decim:end]);
    @show sum( xor.(p̂[1:λ:end],synth.sequence));
    return audioContent
end

function initFilter(fInput,fOutput)
    ratio = fInput / fOutput;
    ratioFFT = 4;
    fftSize = ratioFFT * nextpow(2,ratio);
    inputFFT = zeros(ComplexF32,fftSize);
    # --- Magnitude
    inputFFT[1:ratioFFT] .= 1;
    # --- Linear phase
    pulsation	= 2*pi*(0:fftSize-1)./fftSize;
	groupDelay	= - (fftSize-1)/2;
    fRep		= inputFFT .* exp.(1im .* groupDelay .* pulsation);
    #
    h = ifft(fRep);
    window = blackman(fftSize);
    #
    h .*= window;
    return h;
end


end
