module DemoTx 


using FFTW 
using DSP
using JLD2 
using Statistics
using TempestRealTime
using AbstractSDRs
include("ConvertAWG.jl")
include("Audio.jl")

struct SynthSignals
    duration::Float64;
    blackRate::Float64;
    freqTones::Array{Float64};
    samplingRate::Float64;
    N:: Int;
    λ::Int;
    burstDuration::Float64;
    nbHops::Int;
    nbSymb::Int;
    sequence::Array{Int};

end


function tx()
    # ----------------------------------------------------
    # --- Parameters
    # ---------------------------------------------------- 
    duration    = 1.0;
    blackRate   = 5e4;                  # Probably useless 
    freqTones   = [700;900;1200;700];
    samplingRate= 4e6;
    N           = 80;
    λ           = 10;
    burstDuration = N * λ / samplingRate;
    nbHops     = 1+Int(duration ÷ burstDuration);
    # ----------------------------------------------------
    # --- Generation of FH
    # ---------------------------------------------------- 
    nbSymb      = Int(floor(duration * samplingRate / length(freqTones))*length(freqTones));
    # --- Create sequence
	sequence = rand(1:N, nbHops);
	# --- Create time domain signal 
    d   = [exp.(2im * π / N * ind * n) for ind ∈ sequence for n ∈ (0:λ * N - 1)];
    # ----------------------------------------------------
    # --- Generation of tones 
    # ---------------------------------------------------- 
    durTones    = nbSymb ÷ length(freqTones);
    c   = [1+exp.(2im * π *freq / samplingRate * n) for freq ∈ freqTones for n ∈ (0:durTones-1)];
    # ----------------------------------------------------
    # --- Mixing
    # ---------------------------------------------------- 
    d .*= c;
    # ----------------------------------------------------
    # --- Saving parameters 
    # ---------------------------------------------------- 
    # --- Save parameters 
    synth = SynthSignals(
        duration,
        blackRate,
        freqTones,
        samplingRate,
        N,
        λ,
        burstDuration,
        nbHops,
        nbSymb,
        sequence,
    );
    # --- create AWG signal 
     convertAWG("file_synth.dat",d);
     @save "struc_synth.jld2" synth;
    return d,synth;
end

function testRx2()
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    sig,synth = tx();
    # --- PHY param 
    nbHop   = synth.nbHops;
    N       = synth.N;
    λ       = synth.λ
    nbEst   = nbHop * λ;
    p̂       = zeros(Int,nbEst);
    # --- Filter 
    audioFreq   = 42e3;
    h           = initFilter(synth.samplingRate,audioFreq);
    decim       = Int(synth.samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = 1024;
    d           = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Cfloat},nbSamples);
    # --- Buffer 
    buffer      = zeros(Complex{Cfloat},nbSamples);
    buffer[1:N] .= sig[1:N];
    # Get a first vector to create what is required 
    P   = plan_fft(buffer);
    V   = initRotor(N,nbSamples);
    out = zeros(Complex{Cfloat}, nbSamples * λ * nbHop)
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    @show size(sig)
    nbSeg = length(sig) ÷ nbSamples;
    @show nbEst,nbSeg
    for n ∈ (1:nbSeg)
        #print(".");
        # --- New buffer with ZP
        d .= sig[ (n-1)*nbSamples .+ (1:nbSamples)]
        #d[1:N] .= sig[ (n-1)*N .+ (1:N)]
        #buffer[1:N]  .= d
        # --- Switch to freq domain 
        toFreq!(sigOut,P,d);
        # --- abs2 
        sigMod .= abs2.(sigOut);
        # --- Decision 
        p̂[n] = decision(sigMod,nbSamples,N);
        # --- Rotor 
        derotor!(sigBB,d,V[:,p̂[n]]);
        out[(n-1)*nbSamples .+ (1:nbSamples)] = sigBB;
    end
    # --- LPF 
    outputFilt = conv(out,h);
    audioContent = abs2.(outputFilt[1:decim:end]);
    @show sum( xor.(p̂[1:λ:end],synth.sequence));
    return audioContent,p̂
end


function normalizePower!(in::AbstractArray)
    in .= in ./ sqrt(mean(abs2.(in)));
end


function testRx()
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    sig,synth = tx();
    # --- PHY param 
    nbHop   = synth.nbHops;
    N       = synth.N;
    λ       = synth.λ
    nbEst   = nbHop * λ;
    p̂       = zeros(Int,nbEst);
    # --- Filter 
    audioFreq   = 42e3;
    h           = initFilter(synth.samplingRate,audioFreq);
    decim       = Int(synth.samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = 1024;
    d           = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Cfloat},N);
    # --- Buffer 
    buffer      = zeros(Complex{Cfloat},nbSamples);
    buffer[1:N] .= sig[1:N];
    # Get a first vector to create what is required 
    P   = plan_fft(buffer);
    V   = initRotor(N,N);
    out = zeros(Complex{Cfloat}, nbSamples * λ * nbHop)
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    @show nbEst
    @show size(sig)
    for n ∈ (1:nbEst)
        #print(".");
        # --- New buffer with ZP
        # d .= sig[ (n-1)*nbSamples .+ (1:nbSamples)]
        d[1:N] .= sig[ (n-1)*N .+ (1:N)]
        #buffer[1:N]  .= d
        # --- Switch to freq domain 
        toFreq!(sigOut,P,d);
        # --- abs2 
        sigMod .= abs2.(sigOut);
        # --- Decision 
        p̂[n] = decision(sigMod,nbSamples,N);
        # --- Rotor 
        derotor!(sigBB,d[1:N],V[:,p̂[n]]);
        out[(n-1)*N .+ (1:N)] = sigBB;
    end
    # --- LPF 
    outputFilt = conv(out,h);
    audioContent = abs2.(outputFilt[1:decim:end]);
    @show sum( xor.(p̂[1:λ:end],synth.sequence));
    return audioContent,p̂
end


function debug2()
	# --- Create the radio object in function
    acqui       = 0.1;
  	carrierFreq		= 770e6;
	gain			= 50.0; 
    samplingRate    =  4e6;
    sdr             = "e310"
	radio			= openSDR(sdr,carrierFreq,samplingRate,gain);
	# --- Print the configuration
	print(radio);
	# --- Init parameters 
	# Get the radio size for buffer pre-allocation
	nbSamples 		= radio.packetSize;
	# We will get complex samples from recv! method
	sig		  = zeros(Complex{Cfloat},nbSamples); 
	# --- Targeting 2 seconds acquisition
	# Init counter increment
	nS		  = 0;
	# Max counter definition
	nbBuffer  = Int(acqui * samplingRate) 
	# --- Timestamp init  
	pInit 			= recv!(sig,radio);
	timeInit  	= time();
	while true
		# --- Direct call to avoid allocation 
		p = recv!(sig,radio);
		# # --- Ensure packet is OK
		# err 	= getError(radio);
		# (p != pInit) && (print("."));
		# --- Update counter
		nS		+= p;
		# --- Interruption 
		if nS > nbBuffer
			break 
		end
    end
    @show tEl = time() - timeInit
    @show Int(nS)
end

function debug()
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    # --- PHY param 
    acqui       = 0.1;
  	carrierFreq		= 770e6;
	gain			= 50.0; 
    samplingRate    =  4e6;
    sdr             = "e310";
	radio			= openSDR(sdr,carrierFreq, samplingRate, gain); 
    # --- Detector parameters 
    nbSamples   = 1016;
    sig           = zeros(Complex{Cfloat},nbSamples);
    # sigOut      = zeros(Complex{Cfloat},nbSamples);
    # sigMod      = zeros(Float32,nbSamples);
    # sigBB       = zeros(Complex{Cfloat},nbSamples);
    # --- Testing radio 
    pInit = recv!(sig,radio);
    # Get a first vector to create what is required 
    @show nbSeg = Int(acqui * samplingRate ÷ nbSamples);
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    nS = 0;
    # @time for n ∈ (1:nbSeg)
        while (true)
        # print(".");
        # --- New buffer with ZP
        p  =recv!(sig,radio);
        nS += p;
        if nS > Int(acqui * samplingRate) 
            break;
        end
    end
    @show nS
    close(radio)
end

function rx2()
    @load "struc_synth.jld2" synth;
    # ----------------------------------------------------
    # --- Radio
    # ---------------------------------------------------- 
	carrierFreq		= 770e6;
	gain			= 25.0; 
    samplingRate    = 4e6;
    sdr             = "e310";
	radio			= openSDR(sdr,carrierFreq, samplingRate, gain); 
    radioSize       = radio.packetSize;
    acqui           = 4;      # Desired time of acquisition in second 
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    # --- PHY param 
    N       = synth.N;
    # λ       = synth.λ
    # --- Filter 
    audioFreq   = 42e3;
    h           = initFilter(samplingRate,audioFreq);
    decim       = Int(samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = 1016;
    bigBuffer   = zeros(Complex{Cfloat},radio.packetSize);
    sig         = zeros(Complex{Cfloat},radio.packetSize);
    d           = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Cfloat},nbSamples);
    # --- Testing radio 
    recv!(sig,radio);
    # Get a first vector to create what is required 
    P                   = plan_fft(sig[1:nbSamples]);
    V                   = initRotor(N,nbSamples);
    bufferPerBigBuffer  = Int( radio.packetSize ÷ nbSamples)
    @show nbSeg = Int(acqui * samplingRate ÷ radio.packetSize);
    # usefull vector init
    out = zeros(Complex{Cfloat}, Int(acqui*samplingRate))
    @show nbSeg * bufferPerBigBuffer
    p̂       = zeros(Int,nbSeg*bufferPerBigBuffer);
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    nS = 0;
    gB = 0;
    for n ∈ (1:nbSeg)
        # We get a big buffer from the radio and compute each subvector
        p  =recv!(sig,radio);
        nS += p;
        for m ∈ (1:bufferPerBigBuffer)
            # --- New buffer with ZP
            d .= sig[ (m-1)*nbSamples .+ (1:nbSamples)]
            # --- Switch to freq domain 
            toFreq!(sigOut,P,d);
            # --- abs2 
            sigMod .= abs2.(sigOut);
            # --- Decision 
            indexChannel = decision(sigMod,nbSamples,N);
            p̂[(n-1)*bufferPerBigBuffer + m]  = indexChannel;
            # --- Rotor 
            derotor!(sigBB,d,V[:,indexChannel]);
            out[gB+(m-1)*nbSamples .+ (1:nbSamples)] = sigBB;
        end
        gB += radio.packetSize;
        if nS > Int(acqui * samplingRate) 
            break;
        end
    end
    # --- LPF 
    outputFilt = conv(out,h);
    audioContent = abs2.(outputFilt[1:decim:end]);
    normalizePower!(audioContent);
    close(radio)
    return audioContent,p̂
end

function rxContinuousPlay()
    @load "struc_synth.jld2" synth;
    # ----------------------------------------------------
    # --- Radio
    # ---------------------------------------------------- 
	carrierFreq		= 770e6;
	gain			= 25.0; 
    samplingRate    = 4e6;
    sdr             = "e310";
	radio			= openSDR(sdr,carrierFreq, samplingRate, gain); 
    radioSize       = radio.packetSize;
    acqui           = 4;      # Desired time of acquisition in second 
    # ----------------------------------------------------
    # --- Audio 
    # ----------------------------------------------------    
    stream = Audio.createAudioBuffer();
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    # --- PHY param 
    N       = synth.N;
    # λ       = synth.λ
    # --- Filter 
    audioFreq   = 42e3;
    h           = initFilter(samplingRate,audioFreq);
    decim       = Int(samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = 1016;
    bigBuffer   = zeros(Complex{Cfloat},radio.packetSize);
    sig         = zeros(Complex{Cfloat},radio.packetSize);
    d           = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Cfloat},nbSamples);
    # --- Testing radio 
    recv!(sig,radio);
    # Get a first vector to create what is required 
    P                   = plan_fft(sig[1:nbSamples]);
    V                   = initRotor(N,nbSamples);
    bufferPerBigBuffer  = Int( radio.packetSize ÷ nbSamples)
    @show nbSeg = Int(acqui * samplingRate ÷ radio.packetSize);
    # usefull vector init
    out             = zeros(Complex{Cfloat}, nbSamples * bufferPerBigBuffer)
    cache           = zeros(Complex{Cfloat}, nbSamples * bufferPerBigBuffer+ length(h)-1)
    nbAudio         = length(cache) ÷ decim;
    audioContent    = zeros(Cfloat,nbAudio)
    @show nbSeg * bufferPerBigBuffer
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    nS = 0;
    gB = 0;
    try
        while true
            # We get a big buffer from the radio and compute each subvector
            p  =recv!(sig,radio);
            nS += p;
            for m ∈ (1:bufferPerBigBuffer)
                # --- New buffer with ZP
                d .= sig[ (m-1)*nbSamples .+ (1:nbSamples)]
                # --- Switch to freq domain 
                toFreq!(sigOut,P,d);
                # --- abs2 
                sigMod .= abs2.(sigOut);
                # --- Decision 
                indexChannel = decision(sigMod,nbSamples,N);
                # --- Rotor 
                derotor!(sigBB,d,V[:,indexChannel]);
                out[(m-1)*nbSamples .+ (1:nbSamples)] = sigBB;
            end
            audioProcessing!(audioContent,out,h,cache,decim)
            Audio.play(stream,audioContent)
            # if nS > Int(acqui * samplingRate) 
                # break;
            # end
        end
    catch exception;
        @info "Interruption after $nS processed samples";
    end
    # --- LPF 
    close(radio);
    close(stream);
end

function audioProcessing!(audioContent,out,h,cache,decim)
    cache      .= conv(out,h);
    audioContent    .= abs2.(cache[1:decim:end]);
    normalizePower!(audioContent);
end


function rx()
    @load "struc_synth.jld2" synth;
    # ----------------------------------------------------
    # --- Radio
    # ---------------------------------------------------- 
	carrierFreq		= 770e6;
	gain			= 25.0; 
    samplingRate    =  4e6;
    sdr             = "e310";
	global radio			= openSDR(sdr,carrierFreq, samplingRate, gain); 
	radioSize       = radio.packetSize;
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    # --- PHY param 
    nbHop   = synth.nbHops;
    N       = synth.N;
    λ       = synth.λ
    nbEst   = nbHop * λ;
    p̂       = zeros(Int,nbEst);
    # --- Filter 
    audioFreq   = 42e3;
    h           = initFilter(synth.samplingRate,audioFreq);
    decim       = Int(synth.samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = 1024;
    d           = zeros(Complex{Cfloat},N);
    sigOut      = zeros(Complex{Cfloat},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Cfloat},nbSamples);
    # --- Testing radio 
    recv!(d,radio);
    # --- Buffer 
    buffer      = zeros(Complex{Cfloat},nbSamples);
    buffer[1:N] .= d[1:N];
    # Get a first vector to create what is required 
    P   = plan_fft(buffer);
    V   = initRotor(N,N);
    out = zeros(Complex{Cfloat}, nbSamples * λ * nbHop)
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    @show nbEst
    for n ∈ (1:15600)
        #print(".");
        # --- New buffer with ZP
        recv!(d,radio)
        buffer[1:N]  .= d
        # --- Switch to freq domain 
        toFreq!(sigOut,P,buffer);
        # --- abs2 
        sigMod .= abs2.(sigOut);
        # --- Decision 
        p̂[n] = decision(sigMod,nbSamples,N);
        # --- Rotor 
        derotor!(sigBB,d,V[:,p̂[n]]);
        out[(n-1)*nbSamples .+ (1:nbSamples)] = sigBB;
    end
    # --- LPF 
    outputFilt = conv(out,h);
    audioContent = abs2.(outputFilt[1:decim:end]);
    @show sum( xor.(p̂[1:λ:end],synth.sequence));
    close(radio);
    return audioContent,p̂
end

function initFilter(fInput,fOutput)
    ratio = fInput / fOutput;
    ratioFFT = 4;
    fftSize = ratioFFT * nextpow(2,ratio);
    inputFFT = zeros(Complex{Cfloat},fftSize);
    # --- Magnitude 
    inputFFT[1:ratioFFT] .= 1;
    # --- Linear phase 
    pulsation	= 2*pi*(0:fftSize-1)./fftSize;
	groupDelay	= - (fftSize-1)/2;			
    fRep		= inputFFT .* exp.(1im .* groupDelay .* pulsation);
    # 
    h = ifft(fRep);
    window = blackman(fftSize);
    # 
    h .*= window;
    return h;
end


end