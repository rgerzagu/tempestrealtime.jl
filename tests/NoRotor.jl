module NoRotor 


using FFTW 
using DSP
using JLD2 
using Statistics
using TempestRealTime
using AbstractSDRs
using DigitalComm

function test_N_noRadio()
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    sig,synth = tx();
    # --- PHY param 
    nbHop   = synth.nbHops;
    N       = synth.N;
    λ       = synth.λ
    nbEst   = nbHop * λ;
    p̂       = zeros(Int,nbEst);
    # --- Filter 
    audioFreq   = 42e3;
    audioRendering = 8e3;
    h           = initFilter(synth.samplingRate,audioFreq);
    decim       = Int(synth.samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = synth.N;
    fftSize     = synth.N;
    d           = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Cfloat},nbSamples);
    # --- Buffer 
    buffer      = zeros(Complex{Cfloat},nbSamples);
    buffer[1:N] .= sig[1:N];
    # Get a first vector to create what is required 
    P   = plan_fft(buffer);
    V   = initRotor(N,nbSamples);
    out = zeros(Complex{Cfloat}, nbSamples * λ * nbHop)
    # --- Double descend parameters 
    # We use only one point per FFT 
    sampleDescent = synth.samplingRate / fftSize;
    secondDecim = Int(floor(sampleDescent / audioRendering));
    h2  = initFilter(sampleDescent,audioRendering)
    # --- New decision parameters 
    overBin = Int(floor(fftSize / synth.N));
    (gapA,gapB) = getAv(overBin);
    cacheDec  = Vector{Cfloat}(undef,fftSize);
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    @show size(sig)
    nbSeg = length(sig) ÷ nbSamples;
    @show nbEst,nbSeg
    nS = 0;
    gB = 0;
    descentVector = Vector{Complex{Cfloat}}(undef,0);
    for n ∈ (1:nbSeg)
            #print(".");
            # --- New buffer with ZP
            d .= sig[ (n-1)*nbSamples .+ (1:nbSamples)]
            # --- Switch to freq domain 
            toFreq!(sigOut,P,d);
            # --- abs2 
            sigMod .= abs2.(sigOut);
            # --- Decision 
            indexChannel,posFFT = decisionAndKeep(sigMod,fftSize,N,gapA,gapB,cacheDec);
            p̂[n]  = indexChannel;
            # --- Masking and extraction 
            push!(descentVector,sigOut[posFFT]);
        end
    # --- LPF 
    outputFilt = conv(descentVector,h2);
    audioContent = real.(outputFilt[1:secondDecim:end]);
    audioContent = normalizePower!(audioContent);
    return audioContent,p̂,descentVector,synth
end

# function test_1024ZP_noRadio()
#     # ----------------------------------------------------
#     # --- FH parameters 
#     # ---------------------------------------------------- 
#     sig,synth = tx();
#     # --- PHY param 
#     nbHop   = synth.nbHops;
#     N       = synth.N;
#     λ       = synth.λ
#     nbEst   = nbHop * λ;
#     p̂       = zeros(Int,nbEst);
#     # --- Filter 
#     audioFreq   = 42e3;
#     audioRendering = 8e3;
#     h           = initFilter(synth.samplingRate,audioFreq);
#     decim       = Int(synth.samplingRate ÷ audioFreq);
#     # --- Detector parameters 
#     nbSamples   = synth.N;
#     sizeFFT     = 1024;
#     d           = zeros(Complex{Cfloat},nbSamples);
#     sigOut      = zeros(Complex{Cfloat},sizeFFT);
#     sigMod      = zeros(Float32,sizeFFT);
#     # --- Buffer 
#     buffer      = zeros(Complex{Cfloat},sizeFFT);
#     buffer[1:N] .= sig[1:N];
#     # Get a first vector to create what is required 
#     P   = plan_fft(buffer);
#     V   = initRotor(N,nbSamples);
#     out = zeros(Complex{Cfloat}, nbSamples * λ * nbHop)
#     # --- Double descend parameters 
#     # We use only one point per FFT 
#     sampleDescent = synth.samplingRate / nbSamples;
#     secondDecim = Int(floor(sampleDescent / audioRendering));
#     h2  = real(initFilter(sampleDescent,audioRendering));
#     # --- New decision parameters 
#     overBin = Int(floor(nbSamples / synth.N));
#     (gapA,gapB) = getAv(overBin);
#     cacheDec  = Vector{Cfloat}(undef,sizeFFT);
#     # ----------------------------------------------------
#     # --- Acquisition
#     # ---------------------------------------------------- 
#     @show size(sig)
#     nbSeg = length(sig) ÷ nbSamples;
#     @show nbEst,nbSeg
#     nS = 0;
#     gB = 0;
#     descentVector = Vector{Complex{Cfloat}}(undef,0);
#     for n ∈ (1:nbSeg)
#             #print(".");
#             # --- New buffer with ZP
#             d .= sig[ (n-1)*nbSamples .+ (1:nbSamples)];
#             buffer[1:nbSamples] .= d;
#             # --- Switch to freq domain 
#             toFreq!(sigOut,P,buffer);
#             # --- abs2 
#             sigMod .= abs2.(sigOut);
#             # --- Decision 
#             indexChannel,posFFT = decisionAndKeep(sigMod,sizeFFT,N,gapA,gapB,cacheDec);
#             p̂[n]  = indexChannel;
#             # --- Masking and extraction 
#             push!(descentVector,sigMod[posFFT]);
#         end
#     # --- LPF 
#     outputFilt = conv(descentVector,h2);
#     audioContent = outputFilt[1:secondDecim:end];
#     audioContent = normalizePower!(audioContent);
#     return audioContent,p̂,descentVector,synth
# end

function test_1024_noRadio()
    # ----------------------------------------------------
    # --- FH parameters 
    # ---------------------------------------------------- 
    sig,synth = tx();
    # We add some noise and impairment 
    sig,_ = addNoise(sig,10)
    # --- PHY param 
    nbHop   = synth.nbHops;
    N       = synth.N;
    λ       = synth.λ
    nbEst   = nbHop * λ;
    p̂       = zeros(Int,nbEst);
    # --- Filter 
    audioFreq   = 42e3;
    audioRendering = 8e3;
    h           = initFilter(synth.samplingRate,audioFreq);
    decim       = Int(synth.samplingRate ÷ audioFreq);
    # --- Detector parameters 
    nbSamples   = 1024;         # Size of buffer processed 
    sizeFFT     = 1024;         # Size of FFT 
    sampPerBuff = 1024;           # Number of samples of buffer used for ZP
    d           = zeros(Complex{Cfloat},nbSamples);
    sigOut      = zeros(Complex{Cfloat},sizeFFT);
    sigMod      = zeros(Float32,sizeFFT);
    # --- Buffer 
    buffer      = zeros(Complex{Cfloat},sizeFFT);
    buffer[1:sampPerBuff] .= sig[1:sampPerBuff];
    # Get a first vector to create what is required 
    P   = plan_fft(buffer);
    V   = initRotor(N,nbSamples);
    out = zeros(Complex{Cfloat}, nbSamples * λ * nbHop)
    # --- Double descend parameters 
    # We use only one point per FFT 
    sampleDescent = synth.samplingRate / nbSamples;
    secondDecim = Int(floor(sampleDescent / audioRendering));
    # h2  = initFilter(sampleDescent,audioRendering);
    h2  = real(initFilter(sampleDescent,audioRendering));
    # --- New decision parameters 
    overBin = Int(round(nbSamples / synth.N));
    (gapA,gapB) = getAv(overBin);
    cacheDec  = Vector{Cfloat}(undef,sizeFFT);
    # ----------------------------------------------------
    # --- Acquisition
    # ---------------------------------------------------- 
    @show size(sig)
    nbSeg = length(sig) ÷ nbSamples;
    @show nbEst,nbSeg
    nS = 0;
    gB = 0;
    # descentVector = Vector{Complex{Cfloat}}(undef,0);
    descentVector = Vector{Cfloat}(undef,0);
    for n ∈ (1:nbSeg)
            #print(".");
            # --- New buffer with ZP
            d .= sig[ (n-1)*nbSamples .+ (1:nbSamples)];
            buffer[1:sampPerBuff] .= d[1:sampPerBuff];
            # buffer[1:N] .= d[1:N];
            # --- Switch to freq domain 
            toFreq!(sigOut,P,buffer);
            # --- abs2 
            sigMod .= abs2.(sigOut);
            # --- Decision 
            indexChannel,posFFT = decisionAndKeep(sigMod,sizeFFT,N,gapA,gapB,cacheDec);
            p̂[n]  = indexChannel;
            # --- Masking and extraction 
            # push!(descentVector,sigOut[posFFT]);
            push!(descentVector,sigMod[posFFT]);
        end
    # --- LPF 
    outputFilt = conv(descentVector,h2);
    audioContent = outputFilt[1:secondDecim:end];
    # audioContent = abs2.(outputFilt[1:secondDecim:end]);
    audioContent = normalizePower!(audioContent);
    return audioContent,p̂,descentVector,synth
end


function rx()
  # --- PHY param 
  carrierFreq		= 700e6;
  gain			    = 6; 
  samplingRate      = 100e6;
  sdr               = "uhd";
  global radio	    = openSDR(sdr,carrierFreq, samplingRate, gain); 
  acqui             = 2;      # Desired time of acquisition in second  
  @info "Targeted processing duration => $acqui seconds"
  # --- Parameters 
  N               = 100;    # Number of channels
  # --- Filter 
  audioRendering = 8e3;     # Targeted audio rendering
  # --- Detector parameters 
  nbSamples   = 1024;         # Size of buffer processed 
  sizeFFT     = 1024;         # Size of FFT 
  sampPerBuff = 80;           # Number of samples of buffer used for ZP
  # --- Init all buffers
  sig         = zeros(Complex{Cfloat},nbSamples);
  sigOut      = zeros(Complex{Cfloat},sizeFFT);
  sigMod      = zeros(Float32,sizeFFT);
  # --- Init FFT
  # We get a buffer from the radio to find the best FFT architecture
  recv!(sig,radio);
  buffer      = zeros(Complex{Cfloat},sizeFFT);
  buffer[1:sampPerBuff] .= sig[1:sampPerBuff];
  # Get a first vector to create what is required 
  P   = plan_fft(buffer);
  # --- Double descend parameters 
  # We use only one point per FFT 
  sampleDescent = samplingRate / nbSamples;
  secondDecim   = Int(floor(sampleDescent / audioRendering));
  h2            = real(initFilter(sampleDescent,audioRendering));
  # --- New decision parameters 
  overBin       = Int(floor(nbSamples / N));
  (gapA,gapB)   = getAv(overBin);
  cacheDec      = Vector{Cfloat}(undef,sizeFFT);
  # ----------------------------------------------------
  # --- Acquisition
  # ---------------------------------------------------- 
  nbSeg = acqui * samplingRate ÷ nbSamples;
  nS = 0;
  descentVector = Vector{Cfloat}(undef,0);
  p̂             = Vector{Cfloat}(undef,0);
  @time for n ∈ (1:nbSeg)
          # --- New buffer with ZP
          nS += recv!(sig,radio);
          populateBuffer!(buffer,sig,sampPerBuff)
          # --- Switch to freq domain 
          toFreq!(sigOut,P,buffer);
          # --- abs2 
          sigMod .= abs2.(sigOut);
          # --- Decision 
          indexChannel,posFFT = decisionAndKeep(sigMod,sizeFFT,N,gapA,gapB,cacheDec);
          # --- Masking and extraction 
          push!(p̂,indexChannel);
          push!(descentVector,sigMod[posFFT]);
      end
  # --- LPF 
  # Aliasing filter 
  outputFilt    = conv(descentVector,h2);
  # Decimation 
  audioContent = outputFilt[1:secondDecim:end];
  # Power normalisation
  audioContent = normalizePower!(audioContent);
  # --- Closing radio ressource and output
  close(radio)
  return (audioContent,p̂,outputFilt)
end

function populateBuffer!(buffer,sig,sampPerBuff)
    @inbounds @simd for n = 1 : 1 : sampPerBuff
        buffer[n] = sig[n];
    end
end


end