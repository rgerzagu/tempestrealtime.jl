
# ----------------------------------------------------
# --- Define functions 
# ---------------------------------------------------- 
# --- Ensure data is a Q(16,0,15)
function unitaryScale(d::AbstractArray)
	q   = 15;
	maxVal = 1 - 2.0^(-q);
	return d ./ maximum(abs.(d)) .* maxVal;
end
# --- Conversion to Int 
function toInt(x)
	return Int16(floor(x * 2^15))
end   
function convertAWG(filename::String,d)
	# --- Application to data 
	dScale = unitaryScale(d);
	dInter = zeros(Int16, length(d) * 2);
	for n in 1:length(d)
		dInter[2 * (n - 1) + 1] = toInt(real(dScale[n]));
		dInter[2 * (n - 1) + 2] = toInt(imag(dScale[n]));
	end
    dFinal = bswap.(dInter);
    out =  open(filename,"w")
	write(out,dFinal);
	close(out);
end

## ----------------------------------------------------
## --- Applications 
## ---------------------------------------------------- 
## --- Creating signal 
## Here we load a signal stored in a JLD2 file 
#using JDL2 
#@load "sigHadOc.jld2" bufferSend qamMat bitAll;
#d= bufferSend[:]
## --- Create the file for the AWG
#convertAWG("HadOc.dat",d);
