module TempestRealTime

using UHDBindings
using FFTW
using LinearAlgebra
using Statistics
using DSP
using JLD2


include("ConvertAWG.jl");

export SynthSignals
export tx
export txAudio
export initFilter
export normalizePower!
export toFreq!
export decision, decisionAndKeep
export getAv
export initRotor
export derotor!
export radioSim
export radioSimRx


# ----------------------------------------------------
# --- Signal generation
# ----------------------------------------------------
struct SynthSignals
    duration::Float64;
    blackRate::Float64;
    freqTones::Array{Float64};
    samplingRate::Float64;
    N:: Int;
    λ::Int;
    burstDuration::Float64;
    nbHops::Int;
    nbSymb::Int;
    sequence::Array{Int};

end

function txAudio()
    freqTones   = [700;900;1200;700];
    samplingRate = 42e3;
    durTones= 0.5*samplingRate ÷ length(freqTones);
    c   = [1+exp.(2im * π *freq / samplingRate * n) for freq ∈ freqTones for n ∈ (0:durTones-1)];
    return abs2.(c)
end


function tx()
    # ----------------------------------------------------
    # --- Parameters
    # ----------------------------------------------------
    duration    = 0.5;
    blackRate   = 1e6;
    freqTones   = [700;900;1200;700];
    samplingRate= 80e6;
    N           = 80;
    λ           = 60;
    burstDuration = N * λ / samplingRate;
    nbHops     = 1+Int(duration ÷ burstDuration);
    # ----------------------------------------------------
    # --- Generation of FH
    # ----------------------------------------------------
    nbSymb      = Int(floor(duration * samplingRate / length(freqTones))*length(freqTones));
    # --- Create sequence
	sequence = rand(1:N, nbHops);
	# --- Create time domain signal
    d   = [exp.(2im * π / N * ind * n) for ind ∈ sequence for n ∈ (0:λ * N - 1)];
    # ----------------------------------------------------
    # --- Generation of tones
    # ----------------------------------------------------
    durTones    = Int(ceil(length(d) / length(freqTones)));
    c   = [1+exp.(2im * π *freq / samplingRate * n) for freq ∈ freqTones for n ∈ (0:durTones-1)];
    # ----------------------------------------------------
    # --- Mixing
    # ----------------------------------------------------
    @show size(d), size(c)
    d .*= c[1:length(d)];
    # ----------------------------------------------------
    # --- Saving parameters
    # ----------------------------------------------------
    # --- Save parameters
    synth = SynthSignals(
        duration,
        blackRate,
        freqTones,
        samplingRate,
        N,
        λ,
        burstDuration,
        nbHops,
        nbSymb,
        sequence,
    );
    # --- create AWG signal
     convertAWG("file_synth_100.dat",d);
     @save "struc_synth_100.jld2" synth;
    return d,synth,c;
end

function initFilter(fInput,fOutput)
    ratio = fInput / fOutput;
    ratioFFT = 4;
    fftSize = ratioFFT * nextpow(2,ratio);
    inputFFT = zeros(Complex{Cfloat},fftSize);
    # --- Magnitude
    inputFFT[1:ratioFFT] .= 1;
    # --- Linear phase
    pulsation	= 2*pi*(0:fftSize-1)./fftSize;
	groupDelay	= - (fftSize-1)/2;
    fRep		= inputFFT .* exp.(1im .* groupDelay .* pulsation);
    #
    h = ifft(fRep);
    window = blackman(fftSize);
    #
    h .*= window;
    return h;
end



function normalizePower!(in::AbstractArray)
    in .= in ./ sqrt(mean(abs2.(in)));
end



# ----------------------------------------------------
# --- Core processing routines
# ----------------------------------------------------
function getAv(overBin)
	if  iseven(overBin)
		gapB   = Int(overBin÷2) - 1 ;
		gapA   = Int(overBin÷2) ;
	else
		gapB = Int(overBin÷2) ;
		gapA = Int(overBin÷2);
	end
	return (gapB,gapA)
end

function toFreq!(out,P,sig)
    mul!(out,P,sig);
end
function decision(in,sFFT,N)
    p = Int(round(mod(argmax(in) - 1, sFFT) / sFFT * N));
    (p == 0) && (p = N);
    return p;
end
function decisionAndKeep(in,sFFT,N,gapA,gapB,cache)
    # We use the sliding sum here, and returns 2 things
    # -> The position of the channel and the one in the oversamp FFT
	# --- Transform cont to take into account oversampling
    cache .= in;
    interval = gapB - gapA +1;
	# --- Sliding average with element around data of interest
	@inbounds @simd for n ∈  1+gapA: sFFT - gapB
        # cache[n] = mean(@views in[n-gapA:n+gapB]);
        for index ∈  n - gapA : n + gapB
            cache[n] += in[index];
        end
        cache[n] = cache[n] / interval;
    end
    # --- Position in the oversampled grid
    maxPos = mod(argmax(cache) - 1, sFFT);
    (maxPos == 0) && (maxPos = sFFT);
    # --- Position in the channel grid
    p = Int( round(maxPos / sFFT * N));
    # --- Returning positions
    return (p,maxPos)
end
function initRotor(N,nbSamples)
    V = zeros(ComplexF32,nbSamples,N)
    for ind ∈ (1:N)
        V[:,ind] .= [exp(-2im * π / N * ind * n)  for n ∈ (0: nbSamples - 1)];
    end
    return V;
end
function derotor!(y,x,rotor)
    @inbounds @simd for n ∈ (1:length(x))
        y[n] = x[n] * rotor[n];
    end
end

# ----------------------------------------------------
# --- Radio simulation (Debug purpose)
# ----------------------------------------------------
# --- Some shenanigeans to avoid using real radio device (fallback in AbstractSDRs ?)
mutable struct radioSimRx
    samplingRate::Float64;
end
mutable struct radioSim
    rx::radioSimRx
end
function recv!(sig::Array{Complex{T}},radio::radioSim) where T
    sig  .= randn(Complex{T},length(sig));
end
import Base:close;
function close(radio::radioSim);
    @info "closed"
end
function updateSamplingRate!(radio::radioSim,samplingRate)
    radio.samplingRate = samplingRate;
end
# ------------------

# ----------------------------------------------------
# --- Main call
# ----------------------------------------------------
function main(samplingRate=1e6,radio=nothing)
    # ----------------------------------------------------
    # --- Main parameters
    # ----------------------------------------------------
	carrierFreq		= 770e6;
	gain			= 30.0;
    sdr             = "e310";
    closeRadio      = false;
    if isnothing(radio)
        radio			= openUHD(carrierFreq, samplingRate, gain);
        closeRadio = true;
    else
        updateSamplingRate!(radio,samplingRate);
    end
    # --- TEMPEST parameters
    nbSamples       = 1024;
    N               = 80;
    # ----------------------------------------------------
    # --- Monitoring performance
    # ----------------------------------------------------
    # ---
    nbSamplesProcess = 0;
    nbSampMax   = 10*samplingRate;
    # ----------------------------------------------------
    # --- Init all parameters
    # ----------------------------------------------------
    sig         = zeros(Complex{Float32},nbSamples);
    sigOut      = zeros(Complex{Float32},nbSamples);
    sigMod      = zeros(Float32,nbSamples);
    sigBB       = zeros(Complex{Float32},nbSamples);
    # Get a first vector to create what is required
    recv!(sig,radio);
    P   = plan_fft(sig);
    V   = initRotor(N,nbSamples);
    #
    timeInit = time();
    #
    try
        # while(true)
        while(nbSamplesProcess ≤ nbSampMax);
            # --- Get samples
            recv!(sig,radio);
            # --- Switch to freq domain
            toFreq!(sigOut,P,sig);
            # --- abs2
            sigMod .= abs2.(sigOut);
            # --- Decision
            p = decision(sigMod,nbSamples,N);
            # --- Rotor
            derotor!(sigBB,sig,V[:,p]);
            # --- LPF
            # ??
            # --- Rate calculation
            nbSamplesProcess += nbSamples;
        end
    catch exception;
    end
    # --- Get the rate
    tExec = time() - timeInit;
    effectiveRate   = nbSamplesProcess / tExec / 1e6 ;
    radioRate       = radio.rx.samplingRate;
    @show radioRate,effectiveRate
    if closeRadio
        close(radio);
    end
    return (radioRate,effectiveRate);
end


struct Benchmark
    radioRate::Array{Float64};
    effectiveRate::Array{Float64};
end
function bench()
    carrierFreq		= 770e6;
	gain			= 30.0;
    global radio	= openUHD(carrierFreq, 1e6, gain);
    #global radio	= radioSim(1e6);
    radioV          = [1e3;100e3;500e3;1e6:1e6:8e6;16e6;32e6;64e6;80e6;100e6;200e6];
    radioRate       = similar(radioV);
    effectiveRate   = similar(radioV);
    for iN = 1 : 1 : length(radioV)
        (r,e) = main(radioV[iN],radio);
        radioV[iN] = r/1e6;
        effectiveRate[iN] = e;
    end
    close(radio);
    return Benchmark(radioV,effectiveRate);
end

end
